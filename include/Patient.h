/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "TimeEvolution.h"

typedef struct Patient Patient;

/** Creates an empty structure, given a number of species.
*/
extern Patient *Patient_new(unsigned int numSpecies);

extern Patient *Patient_newFromSpeciesIndexes(HashSet const *speciesIdxs);
extern Patient *Patient_newWithTransientPeriod(unsigned int numSpecies, double transientEnd);
extern double Patient_transientPeriod(Patient const *p);

/** Frees the instance pointed to by *thisP and sets *thisP to NULL. */
extern void Patient_free(Patient **thisP);

/** Wipes _this and loads data from file inFilePath into _this.
   Returns 0 on success and an error code (int < 0) in case inFilePath is
   malformed.
*/
extern int Patient_load(Patient *_this, char const *inFilePath, HashSet const *speciesIdxs, bool withHeader);

/** Returns the number of species N in 'this' (species ids go from 1 to N) */
extern unsigned int Patient_numSpecies(const Patient *_this);

/** Returns the time evolution of species 'speciesIdx' (speciesIdx goes from 1
 * to Patient_nbSpecies(_this)) */
extern TimeEvolution *Patient_evolution(const Patient *p, int speciesIdx);


extern bool Patient_hasTransientPeriod(Patient const *p);
extern double Patient_transientPeriod(Patient const *p);
