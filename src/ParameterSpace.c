/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <math.h>

#include <MCLabUtils.h>
#include <RectRealVectorSpace.h>
#include <RealVector.h>

#include "ParameterSpace.h"

#define DEBUG "ParameterSpace"
#define DEBUG_VERBOSE "ParameterSpace Verbose"
#define NUM_INTERVALS_PER_DIM (2)

struct ParameterSpace {
  unsigned int numParams; // redundant
  Array *def;
  //input bounds
  Array *inputLbs;
  Array *inputUbs;
  Array *inputSteps;
  //computed bounds
  Array *stepsToLb;
  Array *stepsToUb;
  Array *lbs;
  Array *ubs;
  Array *steps;
  Array *stepSizes;
  unsigned long numSlices; // redundant
  HashSet *splittedDims;
  Array *serialized;
};

typedef struct ParameterSpace_Distance {
  unsigned int index;
  int distance;
} ParameterSpace_Distance;

int ParameterSpace_Distance_comparator_desc(void const *e1, void const *e2) {
  ParameterSpace_Distance *d1 = (ParameterSpace_Distance *) e1;
  ParameterSpace_Distance *d2 = (ParameterSpace_Distance *) e2;
  if (Numbers_approxEQ(d1->distance, d2->distance)) return 0;
  if (d1->distance > d2->distance) return -1;
  return 1;
}

int ParameterSpace_Distance_comparator_asc(void const *e1, void const *e2) {
  ParameterSpace_Distance *d1 = (ParameterSpace_Distance *) e1;
  ParameterSpace_Distance *d2 = (ParameterSpace_Distance *) e2;
  if (Numbers_approxEQ(d1->distance, d2->distance)) return 0;
  if (d1->distance < d2->distance) return -1;
  return 1;
}

int const ParameterSpaceStructId = 1000;

ParameterSpace *ParameterSpace_newFromProperties(Properties const *options) {
  int err = 0;
  char const *notifyCmd = NULL;
  err = Properties_getString(options, "notifications/cmd", &notifyCmd);
  int numSlaves = -1;
  err = Properties_getInt(options, "numSlaves", &numSlaves);
  Debug_assert(DEBUG_ALWAYS, err == 0, "numSlaves is missing in options");
  unsigned long numSlicesPerSlave = 0;
  err = Properties_getULong(options, "numSlicesPerSlave", &numSlicesPerSlave);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "Missing 'numSlicesPerSlave' in options.\n");

  char const *paramsFilePath = NULL;
  err = Properties_getString(options, "params", &paramsFilePath);
  Debug_assert(DEBUG_ALWAYS, err == 0, "Missing 'params' in options.\n");
  unsigned long maxNumSlices = numSlicesPerSlave * (unsigned long) numSlaves;
  ParameterSpace *pSpace = ParameterSpace_new(paramsFilePath, maxNumSlices);
  return pSpace;
}

ParameterSpace *ParameterSpace_new(char const *fileName,
                                   unsigned long estNumSlices) {
  ParameterSpace *pSpace = calloc(1, sizeof(ParameterSpace));
  pSpace->def = Array_newDouble(10, 10);
  pSpace->inputLbs = Array_newDouble(10, 10);
  pSpace->inputUbs = Array_newDouble(10, 10);
  pSpace->inputSteps = Array_newInt(10, 10);
  CSV *csv = CSV_new(fileName, 1, '\t', 1024);
  Array *fields = NULL;
  char *field = NULL;
  double value = NAN;
  int steps = -1;
  pSpace->numParams = 0;
  while ((fields = CSV_nextRow(csv)) != NULL) {
    Debug_assert(DEBUG_ALWAYS, Array_length(fields) == 4,
                 "Wrong number of fields in the row.\n");
    Array_get(fields, 0, &field);
    StringUtils_toDouble(field, &value);
    free(field);
    Array_add(pSpace->def, &value);
    Array_get(fields, 1, &field);
    StringUtils_toDouble(field, &value);
    free(field);
    Array_add(pSpace->inputLbs, &value);
    Array_get(fields, 2, &field);
    StringUtils_toDouble(field, &value);
    free(field);
    Array_add(pSpace->inputUbs, &value);
    Array_get(fields, 3, &field);
    StringUtils_toInt(field, &steps);
    free(field);
    Array_add(pSpace->inputSteps, &steps);
    Array_free(&fields);
    ++pSpace->numParams;
  }
  CSV_free(&csv);
  Debug_assert(DEBUG_ALWAYS, pSpace->numParams > 0,
               "There must be at least 1 parameter!\n");
  if (pSpace->numParams == 0) {
    Array_free(&pSpace->def);
    Array_free(&pSpace->inputLbs);
    Array_free(&pSpace->inputUbs);
    Array_free(&pSpace->inputSteps);
    free(pSpace);
    return NULL;
  }
  double ub = NAN;
  double lb = NAN;
  double def = NAN;
  steps = -1;
  double step_size = NAN;
  int steps_to_lb = -1;
  int steps_to_ub = -1;
  Array *distances = Array_new(pSpace->numParams,
                               sizeof(ParameterSpace_Distance), 1, NULL, NULL);
  pSpace->stepsToLb = Array_newInt(pSpace->numParams, 1);
  pSpace->stepsToUb = Array_newInt(pSpace->numParams, 1);
  pSpace->lbs = Array_newDouble(pSpace->numParams, 1);
  pSpace->ubs = Array_newDouble(pSpace->numParams, 1);
  pSpace->steps = Array_newInt(pSpace->numParams, 1);
  pSpace->stepSizes = Array_newDouble(10, 10);
  ParameterSpace_Distance distance = {.index = 0, .distance = -1};
  for (unsigned int i = 0; i < pSpace->numParams; i++) {
    Array_get(pSpace->def, i, &def);
    Array_get(pSpace->inputUbs, i, &ub);
    Array_get(pSpace->inputLbs, i, &lb);
    Array_get(pSpace->inputSteps, i, &steps);
    distance.index = i;
    step_size = (ub - lb) / (steps - 1);
    Array_add(pSpace->stepSizes, &step_size);
    // the interval will be splitted in steps_to_lb values
    steps_to_lb = (int) ((def - lb) / step_size) + 1;
    // the interval will be splitted in steps_to_ub values
    steps_to_ub = (int) ((ub - def) / step_size) + 1;
    // recompute bounds as the grid has to be centered using def values
    if (def - (steps_to_lb - 1) * step_size > lb) {
      steps_to_lb++;
      lb = def - (steps_to_lb - 1) * step_size;
    }
    if (def + (steps_to_ub - 1) * step_size < ub) {
      steps_to_ub++;
      ub = def + (steps_to_ub - 1) * step_size;
    }
    steps = steps_to_lb + steps_to_ub - 1;
    Array_set(pSpace->steps, i, &steps);
    Array_add(pSpace->stepsToLb, &steps_to_lb);
    Array_add(pSpace->stepsToUb, &steps_to_ub);
    Array_add(pSpace->lbs, &lb);
    Array_add(pSpace->ubs, &ub);
    distance.distance = (int) fmax(steps_to_lb, steps_to_ub);
    Array_add(distances, &distance);
    Debug_out(
        DEBUG_VERBOSE,
        "Param[%d]=(%g in [%g,%g]/%g, no inputSteps to lb:%d, no inputSteps to ub:%d)\n",
        i, def, lb, ub, step_size, steps_to_lb, steps_to_ub);
  }
  Debug_perform(DEBUG, {
    for (unsigned int i = 0; i < pSpace->numParams; i++) {
      double inputLb = NAN;
      double inputUb = NAN;
      int inputSteps = -1;
      double lb = NAN;
      double ub = NAN;
      int steps = -1;
      int stepsToLb = -1;
      int stepsToUb = -1;
      double def = NAN;
      Array_get(pSpace->inputLbs, i, &inputLb);
      Array_get(pSpace->inputUbs, i, &inputUb);
      Array_get(pSpace->inputSteps, i, &inputSteps);
      Array_get(pSpace->lbs, i, &lb);
      Array_get(pSpace->ubs, i, &ub);
      Array_get(pSpace->steps, i, &steps);
      Array_get(pSpace->stepsToLb, i, &stepsToLb);
      Array_get(pSpace->stepsToUb, i, &stepsToUb);
      Array_get(pSpace->def, i, &def);
      double stepSize = (inputUb - inputLb)/(inputSteps - 1);
      double stepSizeNew = (ub - lb)/(steps - 1);
      double stepSizeToLb = (def - lb)/(stepsToLb - 1);
      double stepSizeToUb = (ub - def)/(stepsToUb - 1);
      fprintf(stderr, "%d values in [%g,%g]/%g --> %d values in [%g, %g]/%g (%d values in [%g,%g]/%g and %d values in [%g, %g]/%g)\n",
          inputSteps, inputLb, inputUb, stepSize, steps, lb, ub, stepSizeNew, stepsToLb, lb, def, stepSizeToLb, stepsToUb, def, ub, stepSizeToUb);
    }
  });
  Array_sort(distances, ParameterSpace_Distance_comparator_asc);
  unsigned int numSplittedDimsMax =
      (unsigned int) (log(estNumSlices) / log(NUM_INTERVALS_PER_DIM));
  unsigned int numSplittedDims = pSpace->numParams < numSplittedDimsMax
                                     ? pSpace->numParams
                                     : numSplittedDimsMax;
  pSpace->numSlices =
      (unsigned long) pow(NUM_INTERVALS_PER_DIM, numSplittedDims);
  unsigned int dimIdx = 0;
  pSpace->splittedDims = HashSet_new();
  for (unsigned int i = 0; i < numSplittedDims; ++i) {
    Array_get(distances, i, &distance);
    dimIdx = distance.index;
    HashSet_add(pSpace->splittedDims, &dimIdx, sizeof(unsigned int));
  }
  Array_free(&distances);
  pSpace->serialized = NULL;
  return pSpace;
}

void ParameterSpace_free(ParameterSpace **pSpacePtr) {
  Debug_assert(DEBUG_ALWAYS, pSpacePtr != NULL, "pSpacePtr == NULL\n");
  ParameterSpace *pSpace = *pSpacePtr;
  if (pSpace == NULL) return;
  if (pSpace->serialized != NULL) Array_free(&pSpace->serialized);
  Array_free(&pSpace->def);
  Array_free(&pSpace->inputLbs);
  Array_free(&pSpace->inputUbs);
  Array_free(&pSpace->stepsToLb);
  Array_free(&pSpace->stepsToUb);
  Array_free(&pSpace->inputSteps);
  Array_free(&pSpace->stepSizes);
  HashSet_free(&pSpace->splittedDims);
  free(*pSpacePtr);
}

void ParameterSpace_fprint(ParameterSpace const *pSpace, FILE *s) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  fprintf(s, "Parameters:\n");
  double v = NAN;
  int step = -1;
  double stepSize = NAN;
  for (unsigned int i = 0; i < pSpace->numParams; ++i) {
    Array_get(pSpace->def, i, &v);
    fprintf(s, "p[%u]: ", i);
    fprintf(s, "def = %g", v);
    Array_get(pSpace->lbs, i, &v);
    fprintf(s, ", min = %g", v);
    Array_get(pSpace->ubs, i, &v);
    fprintf(s, ", max = %g", v);
    Array_get(pSpace->steps, i, &step);
    fprintf(s, ", step = %d", step);
    Array_get(pSpace->stepsToLb, i, &step);
    fprintf(s, ", stepsToLb = %d", step);
    Array_get(pSpace->stepsToUb, i, &step);
    fprintf(s, ", stepsToUb = %d", step);
    Array_get(pSpace->stepSizes, i, &stepSize);
    fprintf(s, ", stepSize = %g\n", stepSize);
  }
  fprintf(s, "Number of intervals per dimension: %u\n", NUM_INTERVALS_PER_DIM);
  fprintf(s, "Indexes of splitted dimensions:");
  HashSetIterator *iter = HashSetIterator_new(pSpace->splittedDims);
  unsigned int const *dimIdxPtr = NULL;
  while ((dimIdxPtr = HashSetIterator_next(iter, NULL)) != NULL) {
    fprintf(s, " %u", *dimIdxPtr);
  }
  HashSetIterator_free(&iter);
  fprintf(s, "\nNumber of slices: %lu\n", pSpace->numSlices);
}

bool ParameterSpace_belongsTo(ParameterSpace const *pSpace, RealVector const *v) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  Debug_assert(DEBUG_ALWAYS, v != NULL, "v == NULL\n");
  Debug_assert(DEBUG_ALWAYS, pSpace->numParams == RealVector_dim(v), "numParams %u != dim(v) %u \n", pSpace->numParams, RealVector_dim(v));
  double lb = NAN;
  double ub = NAN;
  double val = NAN;
  for (unsigned int i = 0; i < pSpace->numParams; i++) {
    val = RealVector_get(v, i);
    Array_get(pSpace->inputLbs, i, &lb);
    Array_get(pSpace->inputUbs, i, &ub);
    if (val < lb || val > ub) return false;
  }
  return true;
}

Array const *ParameterSpace_getDef(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->def;
}

Array const *ParameterSpace_getLbs(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->lbs;
}

Array const *ParameterSpace_getUbs(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->ubs;
}

Array const *ParameterSpace_getInputLbs(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->inputLbs;
}

Array const *ParameterSpace_getInputUbs(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->inputUbs;
}

Array const *ParameterSpace_getInputSteps(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->inputSteps;
}

Array const *ParameterSpace_getSteps(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->steps;
}

Array const *ParameterSpace_getLbSteps(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->stepsToLb;
}

Array const *ParameterSpace_getUbSteps(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->stepsToUb;
}

unsigned long ParameterSpace_getNumSlices(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->numSlices;
}

unsigned int ParameterSpace_getNumParams(ParameterSpace const *pSpace) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  return pSpace->numParams;
}

RectRealVectorSpace *
ParameterSpace_getRectRealVectorSpaceSlice(ParameterSpace const *pSpace,
                                           unsigned long idx) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  Debug_out(DEBUG, "Slice #%lu\n", idx);
  unsigned int numParams = pSpace->numParams;
  double paramFrom = NAN;
  double paramTo = NAN;
  double paramDef = NAN;
  int paramSteps = -1;
  unsigned int splittedParamsIdx = 0;
  Array *from = Array_newDouble(pSpace->numParams, 1);
  Array *to = Array_newDouble(pSpace->numParams, 1);
  Array *steps = Array_newInt(pSpace->numParams, 1);
  for (unsigned int i = 0; i < numParams; ++i) {
    Array_get(pSpace->lbs, i, &paramFrom);
    Array_get(pSpace->ubs, i, &paramTo);
    Array_get(pSpace->def, i, &paramDef);
    Array_get(pSpace->steps, i, &paramSteps);
    int dimIdx = (int) i;
    if (HashSet_contains(pSpace->splittedDims, &dimIdx, sizeof(unsigned int))) {
      unsigned int intervalIdx =
          (unsigned int) (idx / (unsigned long) pow(NUM_INTERVALS_PER_DIM,
                                                    splittedParamsIdx)) %
          NUM_INTERVALS_PER_DIM;
      if (intervalIdx == 0) {
        paramTo = paramDef;
        Array_get(pSpace->stepsToLb, i, &paramSteps);
      } else {
        paramFrom = paramDef;
        Array_get(pSpace->stepsToUb, i, &paramSteps);
      }
      ++splittedParamsIdx;
    }
    Array_set(from, i, &paramFrom);
    Array_set(to, i, &paramTo);
    Array_set(steps, i, &paramSteps);
    Debug_out(DEBUG, "p[%u] %d values in [%g, %g]\n", i, paramSteps, paramFrom, paramTo);
  }
  RealVector *vectorFrom = RealVector_newFromArray(from);
  RealVector *vectorTo = RealVector_newFromArray(to);
  RectRealVectorSpace *slice =
      RectRealVectorSpace_new(vectorFrom, vectorTo, steps);
  RealVector_free(&vectorFrom);
  RealVector_free(&vectorTo);
  Array_free(&from);
  Array_free(&to);
  Array_free(&steps);
  return slice;
}

Array const *ParameterSpace_serialize(void *obj) {
  Debug_assert(DEBUG_ALWAYS, obj != NULL, "obj == NULL\n");
  ParameterSpace *pSpace = (ParameterSpace *) obj;
  Debug_out(DEBUG, "Serializing ParameterSpace\n");
  if (pSpace->serialized == NULL) {
    unsigned int numSplittedDims =
        (unsigned int) HashSet_size(pSpace->splittedDims);
    unsigned int len = 1 + 10 * pSpace->numParams + 1 + numSplittedDims;
    pSpace->serialized = Array_newDouble(len, 10);
    double v = (double) pSpace->numParams;
    int step = -1;
    Array_add(pSpace->serialized, &v);
    for (unsigned int i = 0; i < pSpace->numParams; ++i) {
      Array_get(pSpace->def, i, &v);
      Array_add(pSpace->serialized, &v);
      Array_get(pSpace->inputLbs, i, &v);
      Array_add(pSpace->serialized, &v);
      Array_get(pSpace->inputUbs, i, &v);
      Array_add(pSpace->serialized, &v);
      Array_get(pSpace->lbs, i, &v);
      Array_add(pSpace->serialized, &v);
      Array_get(pSpace->ubs, i, &v);
      Array_add(pSpace->serialized, &v);
      Array_get(pSpace->inputSteps, i, &step);
      v = (double) step + 0.5;
      Array_add(pSpace->serialized, &v);
      Array_get(pSpace->steps, i, &step);
      v = (double) step + 0.5;
      Array_add(pSpace->serialized, &v);
      Array_get(pSpace->stepsToLb, i, &step);
      v = (double) step + 0.5;
      Array_add(pSpace->serialized, &v);
      Array_get(pSpace->stepsToUb, i, &step);
      v = (double) step + 0.5;
      Array_add(pSpace->serialized, &v);
      Array_get(pSpace->stepSizes, i, &v);
      Array_add(pSpace->serialized, &v);
    }
    v = (double) numSplittedDims + 0.5;
    Array_add(pSpace->serialized, &v);
    HashSetIterator *iter = HashSetIterator_new(pSpace->splittedDims);
    unsigned int const *dimIdxPtr = NULL;
    while ((dimIdxPtr = HashSetIterator_next(iter, NULL)) != NULL) {
      v = (double) *dimIdxPtr + 0.5;
      Array_add(pSpace->serialized, &v);
    }
    HashSetIterator_free(&iter);
  }
  Debug_out(DEBUG, "Resulting array length is %u\n",
            Array_length(pSpace->serialized));
  return pSpace->serialized;
}

void *ParameterSpace_newFromSerialized(Array const *serialized) {
  Debug_assert(DEBUG_ALWAYS, serialized != NULL, "serialized == NULL\n");
  Debug_assert(DEBUG_ALWAYS, Array_elemSize(serialized) == sizeof(double),
               "Wrong element size of serialized array\n");
  ParameterSpace *pSpace = calloc(1, sizeof(ParameterSpace));
  double v = NAN;
  int step = -1;
  unsigned int offset = 0;
  Array_get(serialized, offset++, &v);
  pSpace->numParams = (unsigned int) v;
  pSpace->def = Array_newDouble(pSpace->numParams, 1);
  pSpace->inputLbs = Array_newDouble(pSpace->numParams, 1);
  pSpace->inputUbs = Array_newDouble(pSpace->numParams, 1);
  pSpace->inputSteps = Array_newInt(pSpace->numParams, 1);
  pSpace->lbs = Array_newDouble(pSpace->numParams, 1);
  pSpace->ubs = Array_newDouble(pSpace->numParams, 1);
  pSpace->steps = Array_newInt(pSpace->numParams, 1);
  pSpace->stepsToLb = Array_newInt(pSpace->numParams, 1);
  pSpace->stepsToUb = Array_newInt(pSpace->numParams, 1);
  pSpace->stepSizes = Array_newDouble(pSpace->numParams, 1);
  for (unsigned int i = 0; i < pSpace->numParams; ++i) {
    Array_get(serialized, offset++, &v);
    Array_add(pSpace->def, &v);
    Array_get(serialized, offset++, &v);
    Array_add(pSpace->inputLbs, &v);
    Array_get(serialized, offset++, &v);
    Array_add(pSpace->inputUbs, &v);
    Array_get(serialized, offset++, &v);
    Array_add(pSpace->lbs, &v);
    Array_get(serialized, offset++, &v);
    Array_add(pSpace->ubs, &v);
    Array_get(serialized, offset++, &v);
    step = (int) v;
    Array_add(pSpace->inputSteps, &step);
    Array_get(serialized, offset++, &v);
    step = (int) v;
    Array_add(pSpace->steps, &step);
    Array_get(serialized, offset++, &v);
    step = (int) v;
    Array_add(pSpace->stepsToLb, &step);
    Array_get(serialized, offset++, &v);
    step = (int) v;
    Array_add(pSpace->stepsToUb, &step);
    Array_get(serialized, offset++, &v);
    Array_add(pSpace->stepSizes, &v);
  }
  Array_get(serialized, offset++, &v);
  unsigned int numSplittedDims = (unsigned int) v;
  pSpace->splittedDims = HashSet_new(sizeof(unsigned int));
  for (unsigned int i = 0; i < numSplittedDims; ++i) {
    Array_get(serialized, offset++, &v);
    unsigned int d = (unsigned int) v;
    HashSet_add(pSpace->splittedDims, &d, sizeof(unsigned int));
  }
  pSpace->numSlices =
      (unsigned long) pow(NUM_INTERVALS_PER_DIM, numSplittedDims);
  pSpace->serialized = NULL;
  return pSpace;
}
