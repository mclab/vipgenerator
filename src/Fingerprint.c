/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <MCLabUtils.h>
#include "Fingerprint.h"

struct Fingerprint {
  unsigned int numSpecies;
  unsigned int inputs;
  unsigned int externalFactors;
  Array const *features;
  unsigned int dim;
  Array *values; // of int32_t
  bool trim_values_cached;
  int32_t *trim_values;
  uint64_t trim_values_len;
};

uint64_t Fingerprint_trim_len(Fingerprint const *s) {
  return s->trim_values_len;
}

int32_t const *Fingerprint_trim(Fingerprint *s, uint64_t *size) {
  if (s->trim_values_cached) {
    *size = s->trim_values_len * sizeof(int32_t);
    return s->trim_values;
  }
  int32_t const *values = Fingerprint_as_C_array(s);
  Array *lengths = Array_new(s->inputs * s->numSpecies, sizeof(unsigned int), 1, NULL, NULL);
  unsigned int len = s->externalFactors;
  unsigned int offset = s->externalFactors;
  for (unsigned int input = 0; input < s->inputs; input++) {
    unsigned int features_i = 0;
    Array_get(s->features, input, &features_i);
    for (unsigned int species = 0; species < s->numSpecies; species++) {
      offset += features_i;
      unsigned int j = 0;
      for (;j < features_i && values[offset - 1 - j] == 0; ++j);
//        fprintf(stderr, "%u %u %u %u %u/%u\n", input, features_i, species, j, offset - 1 - j, s->dim);
      unsigned int len_u_s = features_i - j;
      if (len_u_s == 0) len_u_s++;
//      fprintf(stderr, "%u\n", len_u_s);
      Array_add(lengths, &len_u_s);
      len += len_u_s;
    }
  }
  int32_t *new_values = calloc(len, sizeof(int32_t));
  unsigned int k = 0;
  memcpy(new_values, values, s->externalFactors * sizeof(int32_t));
  k += s->externalFactors;
  offset = s->externalFactors;
  for (unsigned int input = 0; input < s->inputs; input++) {
    unsigned int features_i = 0;
    Array_get(s->features, input, &features_i);
    for (unsigned int species = 0; species < s->numSpecies; species++) {
      unsigned int len_u_s = 0;
      Array_get(lengths, input * s->numSpecies + species, &len_u_s);
      if (len_u_s == 0) len_u_s++;
      memcpy(new_values + k, values + offset, len_u_s * sizeof(int32_t));
      offset += features_i;
      k += len_u_s;
    }
  }
  Array_free(&lengths);
  *size = len * sizeof(int32_t);
  s->trim_values = new_values;
  s->trim_values_len = len;
  s->trim_values_cached = true;
  return new_values;
}

int32_t const *Fingerprint_as_C_array(Fingerprint const *s) {
  return (int32_t const *) Array_as_C_array(s->values);
}

Fingerprint *Fingerprint_new(unsigned int numSpecies, unsigned int inputs,
                         unsigned int externalFactors, Array const *features) {
  Fingerprint *new = (Fingerprint *) calloc(1, sizeof(Fingerprint));
  new->numSpecies = numSpecies;
  new->inputs = inputs;
  new->externalFactors = externalFactors;
  new->features = features;
  new->dim = externalFactors;
  for (unsigned int i = 0; i < inputs; i++) {
    unsigned int features_i = 0;
    Array_get(features, i, &features_i);
    new->dim += numSpecies * features_i;
  }
  new->values = Array_new(new->dim, sizeof(int32_t), new->dim, NULL, NULL);
  int32_t zero = 0;
  Array_add_n_times(new->values, &zero, new->dim);
  new->trim_values_cached = false;
  new->trim_values_len = 0;
  new->trim_values = NULL;
  return new;
}

unsigned int Fingerprint_length(Fingerprint const *s) { return s->dim; }

uint64_t Fingerprint_size(Fingerprint const *s) {
  return Array_length(s->values) * Array_elemSize(s->values);
}

void Fingerprint_setExternalFactors(Fingerprint const *s,
                                  unsigned int extFactorsIdx, int32_t value) {
  Debug_assert(DEBUG_ALWAYS, s->externalFactors > 0,
               "This signature does not provide setting of parameters.\n");
  Debug_assert(DEBUG_ALWAYS, extFactorsIdx < s->externalFactors,
               "extFactorsIdx == %d, it should be in [0, %d]\n", extFactorsIdx,
               s->externalFactors);
  Array_set(s->values, extFactorsIdx, &value);
}

void Fingerprint_setFeatures(Fingerprint *s, unsigned int speciesIdx,
                           unsigned int inputIdx,
                           int32_t const *values) {
  Debug_assert(DEBUG_ALWAYS, speciesIdx <= s->numSpecies && speciesIdx > 0,
               "speciesIdx == %d, it should be in [1, %d]\n", speciesIdx,
               s->numSpecies);
  Debug_assert(DEBUG_ALWAYS, inputIdx < s->inputs && inputIdx >= 0,
               "inputIdx == %d, it should be in [0, %d)\n", inputIdx,
               s->inputs);
  unsigned int idx = s->externalFactors;
  unsigned int features_i = 0;
  for (unsigned int i = 0; i < inputIdx; i++) {
    features_i = 0;
    Array_get(s->features, i, &features_i);
    idx += s->numSpecies * features_i;
  }
  Array_get(s->features, inputIdx, &features_i);
  idx += (speciesIdx - 1) * features_i;

  for (unsigned int i = 0; i < features_i; i++) {
    int32_t value = values[i];
//    fprintf(stderr, "FILTER species %u input %u i %u idx+i %u: %ld\n", speciesIdx, inputIdx, i, idx + i, value);
    Array_set(s->values, idx + i, &value);
  }
}

void Fingerprint_free(Fingerprint **sP) {
  Debug_assert(DEBUG_ALWAYS, sP != NULL, "sP == NULL\n");
  Fingerprint *s = *sP;
  if (s == NULL) return;
  Array_free(&(s->values));
  if (s->trim_values_cached) {
    free(s->trim_values);
  }
  free(*sP);
}

void Fingerprint_fprint(Fingerprint const *s, FILE *f) {
  Debug_assert(DEBUG_ALWAYS, f != NULL, "f == NULL\n");
  Debug_assert(DEBUG_ALWAYS, s != NULL, "s == NULL\n");
  for (unsigned int i = 0; i < Array_length(s->values); i++) {
    int32_t val = 0;
    Array_get(s->values, i, &val);
    fprintf(stderr, "%d ", val);
  }
  fprintf(stderr, "\n");
}

void Fingerprint_as_C_array_fprint(void const *v, size_t size, FILE *f) {
  Debug_assert(DEBUG_ALWAYS, v != NULL, "v == NULL\n");
  Debug_assert(DEBUG_ALWAYS, f != NULL, "f == NULL\n");
  int32_t len = (int32_t) (size / sizeof(int32_t));
  int32_t const *d = (int32_t const *) v;
  fprintf(f, "%ld", (long) d[0]);
  for (int i = 1; i < len; i++) {
    fprintf(f, ", %ld", (long) d[i]);
  }
}

