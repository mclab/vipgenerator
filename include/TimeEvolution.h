/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

/**
  An instance of evol data structure represents a time evolution, i.e., a real
  function of time represented as a list of samples. Each sample is a <time
  point, value> pair.
 */

typedef struct TimeEvolution TimeEvolution;

//! Creates a new instance
extern TimeEvolution *TimeEvolution_new(int nbTimePoints);

//! Creates a new instance using the same time-points as from
extern TimeEvolution *
TimeEvolution_new_sameTimePoints(TimeEvolution const *from);

//! Frees the instance pointed to by *evolP and sets *evolP to NULL
extern void TimeEvolution_free(TimeEvolution **evolP);

/** Adds a new <timePoint, value> sample to evol.
    Note: if value is NaN, it is stored as such.
    For efficiency reasons, it is required that timePoint is >=
   TimeEvolution_maxTime(). If not, an assertion is raised.
*/
extern void TimeEvolution_push(TimeEvolution *evol, double timePoint,
                        double value);

/** Changes value associated to the i-th time-point to 'value'.
    Note: if value is NaN, it is stored as such.
 */
extern void TimeEvolution_set(TimeEvolution *evol, int i, double value);

//! Returns the number of time-points of evol
extern int TimeEvolution_length(TimeEvolution const *evol);

//! Returns the value of the time-point of the i-th sample
extern double TimeEvolution_timePoint(TimeEvolution const *evol, int i);

extern double TimeEvolution_findArgMaxWithinInterval(TimeEvolution *evol, double startTime, double stopTime);

extern Array *TimeEvolution_argExtrema(TimeEvolution const *evol, Array **argMinimaIdxs, Array **argMaximaIdxs, int distance, int sampling_freq, double start, double stop, bool debug);

//! Returns the value of the min time-point in evol
extern double TimeEvolution_minTime(TimeEvolution const *evol);


//! Returns the value of the max time-point in evol
extern double TimeEvolution_maxTime(TimeEvolution const *evol);

//! Returns the value of the i-th sample
extern double TimeEvolution_value(TimeEvolution const *evol, int i);

//! Wipes evol.
extern void TimeEvolution_clear(TimeEvolution *evol);

//! Returns the average of the values in evol
extern double TimeEvolution_avg(TimeEvolution *evol);

//! Returns the minimum value of evol
extern double TimeEvolution_min(TimeEvolution *evol);

//! Returns the maximum value of evol
extern double TimeEvolution_max(TimeEvolution *evol);

/* Returns the number of NaN values occurring in evol */
extern int TimeEvolution_nbNaN(TimeEvolution const *evol);

/** Returns the index of the latest sample having a timepoint <= t.
  Returns -1 if t < evol->minTime or t > evol->maxTime
*/
extern int TimeEvolution_lastSampleBefore(TimeEvolution const *evol, double t);

extern void TimeEvolution_fprint(TimeEvolution const *tet, FILE *f);

/**

*/
extern double TimeEvolution_Interpolation_nextTimePointWithValue(
    TimeEvolution const *evol, double v, double t);

extern double TimeEvolution_Interpolation_value(TimeEvolution const *evol,
                                         double t);
