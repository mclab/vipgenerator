/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once


#define SignalMetrics_min(s) ((s)->min)
#define SignalMetrics_max(s) ((s)->max)
#define SignalMetrics_avg(s) ((s)->avg)
#define SignalMetrics_L2(s) ((s)->l2_norm)
#define SignalMetrics_energy(s) ((s)->energy)
#define SignalMetrics_zeroLagCrossCorr(s) ((s)->zero_lag_cross_corr)
#define SignalMetrics_sampleVar(s) ((s)->sample_var)

typedef struct SignalMetrics {
  double avg;
  double zero_lag_cross_corr;
  double l2_norm;
  double energy;
  double sample_var;
  double min;
  double max;
} SignalMetrics;

SignalMetrics *SignalMetrics_new();

void SignalMetrics_free(SignalMetrics **sP);

void SignalMetrics_compute(SignalMetrics *s,
                           TimeEvolution_Transformed const *tet1,
                           TimeEvolution_Transformed const *tet2,
                           Range_Double *timeRange);
