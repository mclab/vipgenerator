/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "RealVectorSet.h"
#include "ParameterSpace.h"
#include "Slice.h"

typedef struct Sampler Sampler;

extern Sampler *Sampler_new(RectRealVectorSpace *space, RealVectorSet *set,
                     RndGen *rndGen, Properties const *options);

extern void Sampler_free(Sampler **thisP);

extern void Sampler_sample(Sampler *sampler, RealVector *res);

extern void Sampler_sampleSeq(Sampler *sampler, Array *samples);
