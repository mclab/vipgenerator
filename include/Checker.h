/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

typedef struct Checker Checker;

extern Checker *Checker_new(ParameterSpace const *pSpace, Simulator const *simulator, char const *injections_path, Properties const *checkerOptions);
extern void Checker_free(Checker **thisP);
extern Range_Double const *Checker_timeRange(Checker const *checker);
extern Patient const *Checker_patientDefault(Checker const *checker);
extern unsigned int Checker_numSpecies(Checker const *checker);

extern Array /* of CheckerResult*/ *Checker_admissibility_check(Array /*of Checker*/ *checkers, RealVector *v,
                                  const char *virtualPatientFileNameTmp,
                                  CheckerOutcome *admissibility_check_outcome_ptr,
                                  double *tau_ptr);
