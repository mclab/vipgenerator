/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>

#include "Fingerprint.h"
#include "FilterResult.h"

struct FilterResult {
  Array *fingerprints;
  Array *distances;
};

FilterResult *FilterResult_new(Array *fingerprints, Array *distances) {
  Debug_assert(DEBUG_ALWAYS, fingerprints != NULL, "fingerprints == NULL\n");
  Debug_assert(DEBUG_ALWAYS, distances != NULL, "distances == NULL\n");
  Debug_assert(DEBUG_ALWAYS, Array_length(fingerprints) == Array_length(distances), "fingerprints and distances have different length\n");
  FilterResult *res = calloc(1, sizeof(FilterResult));
  res->fingerprints = fingerprints;
  res->distances = distances;
  return res;
}

void FilterResult_free(FilterResult **resP) {
  Debug_assert(DEBUG_ALWAYS, resP != NULL, "resP == NULL\n");
  FilterResult *res = *resP;
  if (res == NULL) return;
  for (unsigned int i = 0; i < Array_length(res->fingerprints); i++) {
    Fingerprint *f = NULL;
    Array_get(res->fingerprints, i, &f);
    Fingerprint_free(&f);
  }
  Array_free(&res->fingerprints);
  Array_free(&res->distances);
  free(res);
}

Array * FilterResult_fingerprints(FilterResult const *res) {
  Debug_assert(DEBUG_ALWAYS, res != NULL, "res == NULL\n");
  return res->fingerprints;
}

Array * FilterResult_distances(FilterResult const *res) {
  Debug_assert(DEBUG_ALWAYS, res != NULL, "res == NULL\n");
  return res->distances;
}
