/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

typedef struct MC2Layer MC2Layer;

extern MC2Layer *MC2Layer_new(unsigned int id, double delta, Storage *storage);
extern MC2Layer *MC2Layer_newFromProperties(Properties const *p, Storage *storage);
extern Properties *MC2Layer_info(MC2Layer *const layer);
extern void MC2Layer_update(MC2Layer *layer, Fingerprint *f, unsigned int sliceIdx, unsigned int virtualPatientID, double distance);
extern double MC2Layer_delta(MC2Layer const *layer);
extern void MC2Layer_incrAdm(MC2Layer *layer);
extern void MC2Layer_incrErrors(MC2Layer *layer);
extern void MC2Layer_incrFailures(MC2Layer *layer);
extern unsigned int MC2Layer_failures(MC2Layer *layer);
extern void MC2Layer_resetFailures(MC2Layer *layer);
extern void MC2Layer_resetErrors(MC2Layer *layer);
extern void MC2Layer_free(MC2Layer **mc2LayerP);
extern void MC2Layer_fprint(MC2Layer const *layer, FILE *f);
extern char *MC2Layer_toString(MC2Layer const *layer);
