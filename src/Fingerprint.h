/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once
#include <inttypes.h>

typedef struct Fingerprint Fingerprint;

extern uint64_t Fingerprint_trim_len(Fingerprint const *s);
extern int32_t const *Fingerprint_trim(Fingerprint *s, uint64_t *size);
extern int32_t const *Fingerprint_as_C_array(Fingerprint const *s);
extern Fingerprint *Fingerprint_new(unsigned int numSpecies,
                                unsigned int inputs,
                                unsigned int externalFactors,
                                Array const *features);
extern void Fingerprint_free(Fingerprint **fP);
extern unsigned int Fingerprint_length(Fingerprint const *s);
extern uint64_t Fingerprint_size(Fingerprint const *s);
extern void Fingerprint_setExternalFactors(Fingerprint const *s,
                                         unsigned int extFactorsIdx, int32_t value);
extern void Fingerprint_setFeatures(Fingerprint *s, unsigned int speciesIdx,
                                  unsigned int input,
                                  int32_t const *values);
extern void Fingerprint_fprint(Fingerprint const *s, FILE *f);
extern void Fingerprint_as_C_array_fprint(void const *v, size_t size, FILE *f);
