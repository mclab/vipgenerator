/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <limits.h>

#include <MCLabUtils.h>

#include "Workload.h"

#define DEBUG_THIS "Workload"

struct Workload {
  unsigned long idx;
  char idx_str[256];
  unsigned long seed;
  Array *serialized;
};

int const WorkloadStructId = 2000;

Workload *Workload_new(unsigned long idx, RndGen *r) {
  Workload *workload = calloc(1, sizeof(Workload));
  workload->idx = idx;
  sprintf(workload->idx_str, "%lu", idx);
  workload->seed = RndGen_nextUL(r, 1, UINT_MAX - 1);
  workload->serialized = NULL;
  return workload;
}

Workload *Workload_newWithSeed(unsigned long idx, unsigned long seed) {
  Workload *workload = calloc(1, sizeof(Workload));
  workload->idx = idx;
  sprintf(workload->idx_str, "%lu", idx);
  workload->seed = seed;
  workload->serialized = NULL;
  return workload;
}

void Workload_free(Workload **workloadPtr) {
  Debug_assert(DEBUG_ALWAYS, workloadPtr != NULL, "workloadPtr == NULL\n");
  Workload *workload = *workloadPtr;
  if (workload == NULL) return;
  Array_free(&workload->serialized);
  free(*workloadPtr);
}

char *Workload_idxToString(Workload const *workload) {
  Debug_assert(DEBUG_ALWAYS, workload != NULL, "workload == NULL\n");
  return StringUtils_clone(workload->idx_str);
}

unsigned long Workload_idx(Workload const *workload) {
  Debug_assert(DEBUG_ALWAYS, workload != NULL, "workload == NULL\n");
  return workload->idx;
}

unsigned long Workload_seed(Workload const *workload) {
  Debug_assert(DEBUG_ALWAYS, workload != NULL, "workload == NULL\n");
  return workload->seed;
}

Array const *Workload_serialize(void *obj) {
  Debug_assert(DEBUG_ALWAYS, obj != NULL, "obj == NULL\n");
  Workload *workload = (Workload *) obj;
  if (workload->serialized == NULL) {
    workload->serialized = Array_newULong(2, 1);
    Array_add(workload->serialized, &workload->idx);
    Array_add(workload->serialized, &workload->seed);
  }
  return workload->serialized;
}

void *Workload_newFromSerialized(Array const *serialized) {
  Debug_assert(DEBUG_ALWAYS, serialized != NULL, "serialized == NULL\n");
  Debug_assert(DEBUG_ALWAYS, Array_length(serialized) == 2,
               "Wrong length of serialized array\n");
  Debug_assert(DEBUG_ALWAYS,
               Array_elemSize(serialized) == sizeof(unsigned long),
               "Wrong element size of serialized array\n");
  Workload *workload = calloc(1, sizeof(Workload));
  Array_get(serialized, 0, &workload->idx);
  Array_get(serialized, 1, &workload->seed);
  return workload;
}

void Workload_fprint(Workload const *workload, FILE *s) {
  fprintf(s, "idx = %lu, seed = %lu\n", workload->idx, workload->seed);
}
