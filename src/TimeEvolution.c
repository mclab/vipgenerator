/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#define DEBUG_TE "TimeEvolution"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <MCLabUtils.h>
#include "TimeEvolution.h"


struct TimeEvolution {
  Array *timePoints;
  Array *values;

  // cache
  int cacheValid;
  double avg;
  double min;
  double max;

  // aux data outside cache management
  double minTime;
  double maxTime;
  double avgTimeSlotLength;
  double sum_timeSlotLengths;
  int nbNaN;
};


void TimeEvolution_fprint(TimeEvolution const *t, FILE *f) {
  Debug_assert(DEBUG_ALWAYS, t->timePoints != NULL, "timePoints == NULL\n");
  Debug_assert(DEBUG_ALWAYS, t->values != NULL, "values == NULL\n");
  Debug_assert(DEBUG_ALWAYS,
               Array_length(t->timePoints) == Array_length(t->values),
               "len of timepoints different from len of values\n");
  for (int i = 0; i < TimeEvolution_length(t); i++) {
    fprintf(f, "%u, %.17g, %.17g\n", i, TimeEvolution_timePoint(t, i),
            TimeEvolution_value(t, i));
  }
}

void _TimeEvolution_resetCache(TimeEvolution *this) {
  this->cacheValid = 0;
  this->avg = 0;
  this->min = 0;
  this->max = 0;
}

TimeEvolution *TimeEvolution_new(int nbTimePoints) {
  TimeEvolution *new = calloc(1, sizeof(TimeEvolution));
  new->timePoints =
      Array_newDouble((unsigned int) nbTimePoints, (unsigned int) nbTimePoints);
  new->values =
      Array_newDouble((unsigned int) nbTimePoints, (unsigned int) nbTimePoints);
  _TimeEvolution_resetCache(new);
  new->avgTimeSlotLength = 0;
  new->sum_timeSlotLengths = 0;
  new->minTime = 0;
  new->maxTime = 0;
  new->nbNaN = 0;
  return new;
}

TimeEvolution *TimeEvolution_new_sameTimePoints(TimeEvolution const *from) {
  int len = TimeEvolution_length(from);
  TimeEvolution *new = TimeEvolution_new(len);
  for (int t = 0; t < len; t++) {
    double timePoint = TimeEvolution_timePoint(from, t);
    Array_add(new->timePoints, &timePoint);
  }
  new->avgTimeSlotLength = from->avgTimeSlotLength;
  new->sum_timeSlotLengths = from->avgTimeSlotLength;
  new->minTime = from->minTime;
  new->maxTime = from->maxTime;
  new->nbNaN = 0;
  return new;
}

double TimeEvolution_minTime(TimeEvolution const *this) {
  return this->minTime;
}

double TimeEvolution_maxTime(TimeEvolution const *this) {
  return this->maxTime;
}

void TimeEvolution_free(TimeEvolution **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  TimeEvolution *this = *thisP;
  if (this == NULL) return;
  Array_free(&this->timePoints);
  Array_free(&this->values);
  free(this);
  *thisP = NULL;
}

void TimeEvolution_clear(TimeEvolution *this) {
  Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
  Array_clear(this->timePoints);
  Array_clear(this->values);
  _TimeEvolution_resetCache(this);
  this->avgTimeSlotLength = 0;
  this->sum_timeSlotLengths = 0;
  this->minTime = 0;
  this->maxTime = 0;
  this->nbNaN = 0;
}

/*
 * Given a time evolution and a value for distance,
 * it computes timepoints corresponding to time evolution extrema.
 * Moreover if argMinima and/or argMaxima are not NULL it computes
 * timepoints correspinding to time evolution maxima and/or minima.
 */
Array *TimeEvolution_argExtrema(TimeEvolution const *evol, Array **argMinima,
                                Array **argMaxima, int distance, int sampling_freq, double start, double stop, bool debug) {
  Array *argExtrema = Array_newDouble(100, 100);
  if (argMinima != NULL) *argMinima = Array_newInt(100, 100);
  if (argMaxima != NULL) *argMaxima = Array_newInt(100, 100);
  double currentValue = NAN;
  double tp = 0.0;
  int idx = 0;
  int len = TimeEvolution_length(evol);
  for (int t = 0; t < len; t+=sampling_freq) {
    tp = TimeEvolution_timePoint(evol, t);
    if (!(isnan(start) || isnan(stop)) && (tp < start || tp > stop)) continue;
    currentValue = TimeEvolution_value(evol, t);
    if (debug) fprintf(stderr, "t[%d] = %g, te(%g) = %g\n", t, tp, tp, currentValue);
    int min = 0;
    int max = 0;
    int l_len = (int) fmax(t - distance, 0);
    for (int l = t; l > l_len; l-=sampling_freq) {
      double l_value = TimeEvolution_value(evol, l);
      if (l_value > currentValue) min+=sampling_freq;
      else if (l_value < currentValue) max+=sampling_freq;
      if (debug) fprintf(stderr, "\tl = %d, l_value = %g, min=%d, max=%d\n", l, l_value, min, max);
    }
    int r_len = (int) fmin(t + distance, len);
    for (int r = t; r < r_len; r+=sampling_freq) {
      double r_value = TimeEvolution_value(evol, r);
      if (r_value > currentValue) min+=sampling_freq;
      else if (r_value < currentValue) max+=sampling_freq;
      if (debug) fprintf(stderr, "\tr = %d, r_value = %g, min=%d, max=%d\n", r, r_value, min, max);
    }
    if (min == (distance - sampling_freq) * 2) {
      if (debug) fprintf(stderr, "**** %g is argMinima\n", tp);
      if (argMinima != NULL) Array_add(*argMinima, &idx);
      Array_add(argExtrema, &tp);
      idx++;
    } else if (max == (distance - sampling_freq) * 2) {
      if (debug) fprintf(stderr, "**** %g is argMaxima\n", tp);
      if (argMaxima != NULL) Array_add(*argMaxima, &idx);
      Array_add(argExtrema, &tp);
      idx++;
    }
  }
  return argExtrema;
}

double TimeEvolution_findArgMaxWithinInterval(TimeEvolution *this, double startTime, double stopTime) {
  double argMax = 0.0;
  double max = 0.0;
  for (int i = 0; i < TimeEvolution_length(this); i++) {
    if (Numbers_approxG(TimeEvolution_timePoint(this, i), stopTime)) break;
    if (Numbers_approxGE(TimeEvolution_timePoint(this, i), startTime)) {
      double value = TimeEvolution_value(this, i);
      if (Numbers_approxG(value, max)) {
        argMax = TimeEvolution_timePoint(this, i);
        max = value;
      }
    }
  }
  return argMax;
}

void _TimeEvolution_stats(TimeEvolution *this) {
  Debug_out(DEBUG_TE, "_TimeEvolution_stats(): computing min/max/avg values\n");
  double sum = 0;
  int len = TimeEvolution_length(this);
  this->min = INFINITY;
  this->max = -INFINITY;
  double value = 0;
  for (int t = 0; t < len; t++) {
    value = TimeEvolution_value(this, t);
    sum += value;
    if (value < this->min) this->min = value;
    if (value > this->max) this->max = value;
  }
  this->avg = sum / len;
  Debug_out(DEBUG_TE, "min: %g, max: %g, avg: %g\n", this->min, this->max,
            this->avg);
  this->cacheValid = 1;
}

double TimeEvolution_avg(TimeEvolution *this) {
  if (!this->cacheValid) {
    _TimeEvolution_stats(this);
  } else {
    Debug_out(DEBUG_TE, "TimeEvolution_avg(): returning cached avg value\n");
  }
  return this->avg;
}

double TimeEvolution_min(TimeEvolution *this) {
  if (!this->cacheValid) {
    _TimeEvolution_stats(this);
  } else {
    Debug_out(DEBUG_TE, "TimeEvolution_min(): returning cached min value\n");
  }
  return this->min;
}

double TimeEvolution_max(TimeEvolution *this) {
  if (!this->cacheValid) {
    _TimeEvolution_stats(this);
  } else {
    Debug_out(DEBUG_TE, "TimeEvolution_max(): returning cached max value\n");
  }
  return this->max;
}

int _assert_timePointOrdered(TimeEvolution const *this, double timePoint) {
  if (Array_length(this->timePoints) == 0) return 1;
  double last = 0;
  Array_getLast(this->timePoints, &last);
  return timePoint > last;
}

double _TimeEvolution_lastTimeSlotLength(TimeEvolution const *this) {
  unsigned int len = Array_length(this->timePoints);
  if (len < 2) return 0;
  double last = 0;
  Array_getLast(this->timePoints, &last);
  double beforeLast = 0;
  Array_get(this->timePoints, len - 2, &beforeLast);
  return last - beforeLast;
}

void TimeEvolution_push(TimeEvolution *this, double timePoint, double value) {
  Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
  /* Debug_assert(DEBUG_ALWAYS, Numbers_approxGE(timePoint, 0), "timePoint < 0\n"); */
  Debug_assert(DEBUG_ALL, _assert_timePointOrdered(this, timePoint),
               "timePoint <= last time-point of this\n");
  if (Array_length(this->timePoints) == 0) {
    // First timepoint
    this->minTime = timePoint;
  }
  Array_push(this->timePoints, &timePoint);
  Array_push(this->values, &value);
  _TimeEvolution_resetCache(this);
  this->sum_timeSlotLengths += _TimeEvolution_lastTimeSlotLength(this);
  this->avgTimeSlotLength =
      this->sum_timeSlotLengths / Array_length(this->timePoints);
  this->maxTime = timePoint;
  if (isnan(value)) this->nbNaN++;
}
void TimeEvolution_set(TimeEvolution *this, int i, double value) {
  Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");

  double oldValue = TimeEvolution_value(this, i);
  if (isnan(oldValue)) this->nbNaN--;

  Array_set(this->values, (unsigned int) i, &value);
  _TimeEvolution_resetCache(this);
  if (isnan(value)) this->nbNaN++;
}

int TimeEvolution_nbNaN(TimeEvolution const *this) { return this->nbNaN; }

int TimeEvolution_length(TimeEvolution const *this) {
  Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
  Debug_assert(DEBUG_ALWAYS,
               Array_length(this->timePoints) == Array_length(this->values),
               "Array_length(this->timePoints) = %d != "
               "Array_length(this->values) = %d\n",
               Array_length(this->timePoints), Array_length(this->values));
  return (int) Array_length(this->timePoints);
}
double TimeEvolution_timePoint(TimeEvolution const *this, int i) {
  Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
  Debug_assert(DEBUG_ALWAYS, (unsigned int) i < Array_length(this->timePoints),
               "i=%d >= |this->timePoints|=%u\n", i,
               Array_length(this->timePoints));
  double result = 0;
  Array_get(this->timePoints, (unsigned int) i, &result);
  return result;
}
double TimeEvolution_value(TimeEvolution const *this, int i) {
  Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
  Debug_assert(DEBUG_ALWAYS, (unsigned int) i < Array_length(this->values),
               "i=%d >= |this->values|=%d\n", i, Array_length(this->values));
  double result = 0;
  Array_get(this->values, (unsigned int) i, &result);
  return result;
}

#define DEBUG_ADJ_CNTR_WARNING_THRESHOLD 3
int TimeEvolution_lastSampleBefore(TimeEvolution const *this, double t) {
  // Debug_assert(DEBUG_ALL, t <= this->maxTime, "t = %lf > this->maxTime =
  // %lf\n", t, this->maxTime);
  if (Numbers_approxL(t, this->minTime) || Numbers_approxG(t, this->maxTime)) return -1;


  // Find the latest sample whose time-point is <= t
  double sample_idx_double = (t / this->avgTimeSlotLength);
  int sample_idx = (int) sample_idx_double;

  int len = TimeEvolution_length(this);

  if (Numbers_approxG(t, this->maxTime)) sample_idx = len - 1;

  int adjustingCounter = 0;
  // If guess was to the right of the correct result, move left
  while (sample_idx >= len || Numbers_approxG(TimeEvolution_timePoint(this, sample_idx), t)) {
    sample_idx--;
    adjustingCounter++;
  }
  if (sample_idx == len - 1) {
    // sample_idx is the last sample:
    // as t <= this->maxTime, this means that t == this->maxTime
    if (adjustingCounter >= DEBUG_ADJ_CNTR_WARNING_THRESHOLD) {
      fprintf(stderr,
              "\nWARNING - TimeEvolution_lastSampleBefore(): "
              "Too many adjustments (%d) to find the right sample: "
              "time-evolution is not uniformly sampled and this leads "
              "to inefficiency\n",
              adjustingCounter);
    }
    return sample_idx;
  }

  while (sample_idx < len - 1 &&
         Numbers_approxLE(TimeEvolution_timePoint(this, sample_idx + 1), t)) {
    sample_idx++;
    adjustingCounter++;
  }
  if (adjustingCounter >= DEBUG_ADJ_CNTR_WARNING_THRESHOLD) {
    fprintf(stderr,
            "\nWARNING - TimeEvolution_lastSampleBefore(): "
            "Too many adjustments (%d) to find the right sample: "
            "time-evolution is not uniformly sampled and this leads to "
            "inefficiency\n",
            adjustingCounter);
  }

  Debug_assert(
      DEBUG_TE,
      Numbers_approxLE(TimeEvolution_timePoint(this, sample_idx), t) &&
          Numbers_approxL(t, TimeEvolution_timePoint(this, sample_idx + 1)),
      "Wrong computation of latest sample referring to time point before %.17g: "
      "sample_idx = %d but timePoint(sample_idx) = %.17g and "
      "timePoint(sample_idx+1) = %.17g\n",
      t, sample_idx, TimeEvolution_timePoint(this, sample_idx),
      TimeEvolution_timePoint(this, sample_idx + 1));
  return sample_idx;
}

double
TimeEvolution_Interpolation_nextTimePointWithValue(TimeEvolution const *this,
                                                   double v, double t) {

  int i = TimeEvolution_lastSampleBefore(this, t);
  int len = TimeEvolution_length(this);

  for (i = i + 2; i < len; i++) {
    int i_prev = i - 1;
    double value_i = TimeEvolution_value(this, i);
    double value_i_prev = TimeEvolution_value(this, i_prev);
    if ((value_i_prev <= v && v <= value_i) ||
        (value_i <= v && v <= value_i_prev)) {
      double timeSlotLength = TimeEvolution_timePoint(this, i) -
                              TimeEvolution_timePoint(this, i_prev);
      double slope = (value_i - value_i_prev) / timeSlotLength;

      /*v =  value_i_prev + slope * ( delta_t ) , with delta_t between 0 and
      timeSlotLength delta_t = (v - value_i_prev)/slope;
      */
      return TimeEvolution_timePoint(this, i_prev) + (v - value_i_prev) / slope;
    }
  }
  // not found
  if (t - 1 >= -1)
    return -1;
  else
    return t - 1;
}


double TimeEvolution_Interpolation_value(TimeEvolution const *this, double t) {
  int sample_idx = TimeEvolution_lastSampleBefore(this, t);
  double value_sample_idx = TimeEvolution_value(this, sample_idx);

  if (sample_idx == TimeEvolution_length(this) - 1) {
    return value_sample_idx;
  }

  double value_sample_idx_plus_1 = TimeEvolution_value(this, sample_idx + 1);

  double timeSlotLength = TimeEvolution_timePoint(this, sample_idx + 1) -
                          TimeEvolution_timePoint(this, sample_idx);

  double slope = (value_sample_idx_plus_1 - value_sample_idx) / timeSlotLength;
  double delta_t = t - TimeEvolution_timePoint(this, sample_idx);
  return value_sample_idx + slope * delta_t;
}
