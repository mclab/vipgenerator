/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

typedef struct Storage Storage;
typedef struct StorageEntry StorageEntry;
typedef struct StorageIterator StorageIterator;

typedef enum StorageMode {
  STORAGE_MODE_CREATE,
  STORAGE_MODE_OPEN
} StorageMode;

extern Storage *Storage_new(unsigned int numParams, char const *path,
                            StorageMode mode);
extern void Storage_free(Storage **storageP);
extern void Storage_fprint(Storage *storage, FILE *f);
extern Array /*RealVector*/ *Storage_getVirtualPatientParametersOfSlice(Storage const *storage, unsigned int sliceIdx);
extern void Storage_beginTransaction(Storage *storage);
extern void Storage_endTransaction(Storage *storage);
extern void Storage_addParameter(Storage *storage, unsigned int sliceIdx, unsigned int id, RealVector const *v);
extern void Storage_addVirtualPatient(Storage *storage, unsigned int sliceIdx, unsigned int id, double tau, RealVector const *v);
extern void Storage_linkFingerprintVirtualPatient(Storage *storage, unsigned int sliceIdx, unsigned int layer, unsigned int id, Fingerprint *fingerprint, double distance);
extern bool Storage_addFingerprint(Storage *storage, unsigned int layer, Fingerprint *fingerprint);
extern bool Storage_containsFingerprint(Storage *storage, unsigned int layer, Fingerprint *fingerprint);

extern StorageIterator *StorageIterator_new(Storage const *storage, char *sql_query);
extern RealVector *StorageIterator_next(StorageIterator *it);
extern bool StorageIterator_hasNext(StorageIterator *it);
extern void StorageIterator_free(StorageIterator **itP);
