/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdio.h>

#include <MCLabUtils.h>
#include <RectRealVectorSpace.h>


typedef struct ParameterSpace ParameterSpace;

extern ParameterSpace *ParameterSpace_newFromProperties(Properties const *options);

extern ParameterSpace *ParameterSpace_new(char const *fileName,
                                          unsigned long estNumSlices);

extern void ParameterSpace_free(ParameterSpace **pSpacePtr);

extern bool ParameterSpace_belongsTo(ParameterSpace const *pSpace, RealVector const *v);

extern unsigned long ParameterSpace_getNumSlices(ParameterSpace const *pSpace);

extern Array const *ParameterSpace_getDef(ParameterSpace const *pSpace);

extern Array const *ParameterSpace_getLbs(ParameterSpace const *pSpace);

extern Array const *ParameterSpace_getUbs(ParameterSpace const *pSpace);

extern Array const *ParameterSpace_getInputLbs(ParameterSpace const *pSpace);

extern Array const *ParameterSpace_getInputUbs(ParameterSpace const *pSpace);

extern Array const *ParameterSpace_getSteps(ParameterSpace const *pSpace);

extern Array const *ParameterSpace_getInputSteps(ParameterSpace const *pSpace);

extern unsigned int ParameterSpace_getNumParams(ParameterSpace const *pSpace);

extern RectRealVectorSpace *
ParameterSpace_getRectRealVectorSpaceSlice(ParameterSpace const *pSpace,
                                           unsigned long idx);

extern void ParameterSpace_fprint(ParameterSpace const *pSpace, FILE *s);

extern Array const *ParameterSpace_serialize(void *obj);

extern void *ParameterSpace_newFromSerialized(Array const *serialized);

extern int const ParameterSpaceStructId;
