/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>

#include <sqlite3.h>

#include <MCLabUtils.h>

#include "db.h"

void sql_exec(sqlite3 *db, char const *sql) {
  int status;
  char *errmsg;
  status = sqlite3_exec(db, sql, NULL, NULL, &errmsg);
  Debug_assert(DEBUG_ALWAYS, status == SQLITE_OK, "Error during sql exec %s (\n%s)\n", errmsg, sql);
}

uint32_t sql_hash(void const *key, size_t len)
{
  char const *key_c = (char const *) key;
  uint32_t hash, i;
  for (hash = i = 0; i < len; ++i) {
    hash += (uint32_t) key_c[i];
    hash += (hash << 10);
    hash ^= (hash >> 6);
  }
  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);
  return hash;
}

