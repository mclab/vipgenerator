/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <zlib.h>

#include <MCLabUtils.h>
#include <RealVector.h>


#include "Workload.h"
#include "RealVectorSet.h"
#include "Sampler.h"
#include "Fingerprint.h"
#include "Patient.h"
#include "Injections.h"
#include "Simulator.h"
#include "CheckerOutcome.h"
#include "CheckerResult.h"
#include "Checker.h"
#include "FilterResult.h"
#include "Filter.h"
#include "Storage.h"
#include "MC2Layer.h"
#include "MC2.h"

#define DEBUG_THIS "MC2"
#define DEBUG_THIS_MEMORY "MC2 Memory"
#define DEBUG_VERBOSE "MC2 Verbose"
#define DEBUG_VERBOSE_SET "MC2 Verbose RealVectorSet"
#define MC2_TIME "MC2 Time"

struct MC2 {
  int id;
  unsigned int numSpecies;
  double delta;
  unsigned int n; // handles the sampler policy update
  ParameterSpace const *pSpace;
  Array * /*Checker*/ checkers;
  Filter *filter;
  RndGen *rndGen;
  Storage *storage;
  char *outputPath;
  char resultFilePath[FILENAME_MAX];
  char virtualPatientOutputPath[FILENAME_MAX];
  Array *samples;

  Array *layers; // of MC2Layers

  Properties *options;
  char snapshotFilename[FILENAME_MAX];
  unsigned int outputStream_count;

  double sum_checker_run_time;
  bool recoverFromSnapshot;
};

static void update_layers(MC2 *mc2, Workload const *workload, unsigned long nTrials, Array *v_discr, Array *checker_results);

static void makeSnapshot(MC2 *mc2, unsigned long nTrials,
                         unsigned long nSimulations,
                         unsigned int virtualPatients);

static Array *initSampleArray(unsigned int n, unsigned int numParams) {
  Array *samples = Array_new(n, sizeof(RealVector *), 1, NULL, NULL);
  for (unsigned int i = 0; i < n; i++) {
    RealVector *v = RealVector_new(numParams);
    Array_add(samples, &v);
  }
  return samples;
}

MC2 *MC2_new(int id, ParameterSpace const *pSpace, Array * /*Checker*/ checkers,
             char const *outputPath, double delta,
             Properties const *options) {
  MC2 *mc2 = calloc(1, sizeof(MC2));
  mc2->options = Properties_clone(options);
  sprintf(mc2->snapshotFilename, "%s/snapshot.prop.txt", outputPath);

  int err = -1;

  // Check if options contains a snapshot nested properties
  Properties *snapshot = NULL;
  mc2->recoverFromSnapshot = true;
  err = Properties_getNestedProperties(options, "snapshot", &snapshot);
  if (err != 0) {
    snapshot = NULL;
    mc2->recoverFromSnapshot = false;
  }
  err = Properties_getUInt(options, "sampler policy revision", &mc2->n);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'sampler policy revision' missing in options\n");

  mc2->id = id;
  mc2->pSpace = pSpace;
  mc2->rndGen = RndGen_new();
  mc2->checkers = checkers;
  mc2->delta = delta;
  mc2->samples = initSampleArray(mc2->n, ParameterSpace_getNumParams(pSpace));
  Checker *checker = NULL;
  Array_get(mc2->checkers, 0, &checker);
  mc2->numSpecies = Checker_numSpecies(checker);

  Properties *filterOptions = NULL;
  err =
      Properties_getNestedProperties(options, "filterOptions", &filterOptions);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'filterOptions' missing in options\n");
  Array *def = Array_new(Array_length(checkers), sizeof(Patient *), 1, NULL, NULL);
  for (unsigned int i = 0; i < Array_length(checkers); i++) {
    Checker *c = NULL;
    Array_get(checkers, i, &c);
    Patient const *p = Checker_patientDefault(c);
    Array_add(def, &p);
  }
  mc2->filter = Filter_new(mc2->numSpecies, def, filterOptions);
  Array_free(&def);

  unsigned int numLayers = Filter_layers(mc2->filter) + 1;
  Properties *layersProp = NULL;
  if (mc2->recoverFromSnapshot) {
    /*   // check if the number of layers in the snapshot properties is equal to
     * the expected one */
    err = Properties_getNestedProperties(snapshot, "layers", &layersProp);
    Debug_assert(DEBUG_ALWAYS, err == 0, "layers missing from snapshot");
    Debug_assert(DEBUG_ALWAYS, numLayers == Properties_size(layersProp),
                 "Expected num of layers %u, instead %lu\n", numLayers,
                 Properties_size(layersProp));
  }

  mc2->outputPath = StringUtils_clone(outputPath);
  sprintf(mc2->resultFilePath, "%s/result.db", outputPath);

  StorageMode storageMode = STORAGE_MODE_CREATE;
  if (mc2->recoverFromSnapshot) {
    storageMode = STORAGE_MODE_OPEN;
  }
  mc2->storage = Storage_new(ParameterSpace_getNumParams(mc2->pSpace),
                             mc2->resultFilePath, storageMode);

  mc2->layers = Array_new(numLayers, sizeof(MC2Layer *), 1, NULL, NULL);
  MC2Layer *layer = NULL;
  for (unsigned int i = 0; i < numLayers; i++) {
    char layerIdxStr[FILENAME_MAX] = {0};
    sprintf(layerIdxStr, "%u", i);
    if (mc2->recoverFromSnapshot) {
      Properties *layerProp = NULL;
      Properties_getNestedProperties(layersProp, layerIdxStr, &layerProp);
      Debug_assert(DEBUG_ALWAYS, layerProp != NULL, "layerProp == NULL\n");
      layer = MC2Layer_newFromProperties(layerProp, mc2->storage);
    } else {
      layer = MC2Layer_new(i, mc2->delta, mc2->storage);
    }
    Array_add(mc2->layers, &layer);
  }

  sprintf(mc2->virtualPatientOutputPath, "%s/%s", outputPath,
          "virtualPatients");
  err = mkdir(mc2->virtualPatientOutputPath, S_IRWXU | S_IRWXG | S_IRWXO);
  Debug_assert(DEBUG_ALWAYS, errno == EEXIST || err == 0,
               "Cannot create output directory for worker #%d\n", id);
  return mc2;
}

void MC2_free(MC2 **mc2Ptr) {
  Debug_assert(DEBUG_ALWAYS, mc2Ptr != NULL, "mc2Ptr == NULL\n");
  MC2 *mc2 = *mc2Ptr;
  if (mc2 == NULL) return;
  RndGen_free(&mc2->rndGen);
  RealVector *v = NULL;
  for (unsigned int i = 0; i < mc2->n; i++) {
    Array_get(mc2->samples, i, &v);
    RealVector_free(&v);
  }
  Array_free(&mc2->samples);
  Filter_free(&mc2->filter);
  MC2Layer *layer = NULL;
  for (unsigned int i = 0; i < Array_length(mc2->layers); i++) {
    Array_get(mc2->layers, i, &layer);
    MC2Layer_free(&layer);
  }
  Array_free(&mc2->layers);
  Storage_free(&mc2->storage);
  Properties_free(&mc2->options);
  free(mc2->outputPath);
  free(*mc2Ptr);
}

static void recoverFromSnapshot(MC2 *mc2, unsigned long sliceIdx,
                                unsigned long *nTrials,
                                unsigned long *nSimulations,
                                RealVectorSet *virtualPatients) {
  Properties *snapshot = NULL;
  int err = Properties_getNestedProperties(mc2->options, "snapshot", &snapshot);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'snapshot' missing in options\n");
  unsigned int virtualPatientsSize = 0;
  err = Properties_getUInt(snapshot, "virtualPatients", &virtualPatientsSize);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "'virtualPatients' missing in snapshot\n");
  err = Properties_getULong(snapshot, "nTrials", nTrials);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'nTrials' missing in snapshot\n");
  err = Properties_getULong(snapshot, "nSimulations", nSimulations);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'nSimulations' missing in snapshot\n");
  unsigned long seed = 0;
  err = Properties_getULong(snapshot, "seed", &seed);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'seed' missing in snapshot\n");
  double walltime = 0;
  err = Properties_getDouble(snapshot, "walltime", &walltime);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'walltime' missing in snapshot\n");
  err = Properties_getUInt(mc2->options, "snapshot/outputStream_count", &mc2->outputStream_count);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'outputStream_count' missing in snapshot\n");

  Array *vps = Storage_getVirtualPatientParametersOfSlice(
      mc2->storage, (unsigned int) sliceIdx);
  Debug_assert(DEBUG_ALWAYS, Array_length(vps) == virtualPatientsSize,
               "%u != %u\n", Array_length(vps), virtualPatientsSize);
  RealVector *v = NULL;
  for (unsigned int i = 0; i < Array_length(vps); i++) {
    Array_get(vps, i, &v);
    RealVectorSet_add(virtualPatients, sizeof(double) * RealVector_dim(v),
                      Array_as_C_array(RealVector_as_Array(v)), v);
    RealVector_free(&v);
  }
  Array_free(&vps);
  Debug_assert(DEBUG_ALWAYS,
               (unsigned int) RealVectorSet_size(virtualPatients) ==
                   virtualPatientsSize,
               "%u != %u\n", (unsigned int) RealVectorSet_size(virtualPatients),
               virtualPatientsSize);

  RndGen_setSeed(mc2->rndGen, seed);
  Timer_end(MC2_TIME);
  Timer_startFromValue(MC2_TIME, TIMER_TO_MSEC(walltime));
}

void MC2_run(MC2 *mc2, Workload const *workload) {
  Debug_assert(DEBUG_ALWAYS, mc2 != NULL, "mc2 == NULL\n");
  Timer_start(MC2_TIME);
  Debug_out(DEBUG_THIS, "Starting MC2 run for slice #%lu and seed %lu\n",
            Workload_idx(workload), Workload_seed(workload));
  RndGen_setSeed(mc2->rndGen, Workload_seed(workload));
  RealVectorSet *virtualPatients = RealVectorSet_new_withRndAccess();
  RealVector *def = RealVector_newFromArray(ParameterSpace_getDef(mc2->pSpace));
  RectRealVectorSpace *slice = ParameterSpace_getRectRealVectorSpaceSlice(
      mc2->pSpace, Workload_idx(workload));
  char *idx_str = Workload_idxToString(workload);
  unsigned long nTrials = 0;
  unsigned long nSimulations = 0;
  Sampler *sampler =
      Sampler_new(slice, virtualPatients, mc2->rndGen, mc2->options);
  MC2Layer *layer = NULL;
  RealVector *v = NULL;
  Array *v_discr = Array_newInt(ParameterSpace_getNumParams(mc2->pSpace), 1);
  int zero = 0;
  Array_add_n_times(v_discr, &zero, ParameterSpace_getNumParams(mc2->pSpace));
  if (mc2->recoverFromSnapshot) {
    recoverFromSnapshot(mc2, Workload_idx(workload), &nTrials, &nSimulations,
                        virtualPatients);
    for (unsigned int i = 0; i < Array_length(mc2->layers); i++) {
      Array_get(mc2->layers, i, &layer);
      MC2Layer_resetFailures(layer);
    }
    layer = NULL;
  } else {
    RealVectorSet_add(virtualPatients, sizeof(double) * RealVector_dim(def),
                      Array_as_C_array(RealVector_as_Array(def)), def);
    Storage_addVirtualPatient(
        mc2->storage, (unsigned int) Workload_idx(workload), 0, 0.0, def);
    char *virtualPatientFileNameTmp =
        StringUtils_concatenate(2, mc2->virtualPatientOutputPath, "/default.csv");
    CheckerOutcome admissibility_check_outcome = CHECKER_OUTCOME_ERR;
    double tau = NAN;
    Array *checker_results =
        Checker_admissibility_check(mc2->checkers, def, virtualPatientFileNameTmp,
                            &admissibility_check_outcome, &tau);
    Debug_assert(DEBUG_ALWAYS, admissibility_check_outcome == CHECKER_OUTCOME_ADM, "Error default is not admissible.\n");
    RectRealVectorSpace_discretisedRealVectorIndexes(slice, def, v_discr);
    update_layers(mc2, workload, nTrials, v_discr, checker_results);
    for (unsigned int j = 0; j < Array_length(checker_results); j++) {
      CheckerResult *res = NULL;
      Array_get(checker_results, j, &res);
      CheckerResult_free(&res);
    }
    Array_free(&checker_results);
  }

  while (true) {
    char *outputStreamFileName = NULL;
    asprintf(&outputStreamFileName, "%s/output.%u.csv.gz", mc2->outputPath, mc2->outputStream_count);
    gzFile outputStream = gzopen(outputStreamFileName, "w");
    gzprintf(outputStream, "timestamp,trials,sample,layer,adm,epsilon\n");
    // Policy Revision Start
    Storage_beginTransaction(mc2->storage);
    Sampler_sampleSeq(sampler, mc2->samples);
    bool revisePolicy = true;
    for (unsigned int i = 0; i < Array_length(mc2->layers); i++) {
      layer = NULL;
      Array_get(mc2->layers, i, &layer);
      if (i == 0 && MC2Layer_failures(layer) == mc2->n) revisePolicy = false; // there is no need to revise the policy (no vp has been found in previous round)
      if (revisePolicy) MC2Layer_resetFailures(layer);
    }
    // Policy Revision End
    // Start admissibility checks with the current sampler policy
    for (unsigned int sample = 0; sample < mc2->n; sample++) {
      Array_get(mc2->samples, sample, &v);
      fprintf(stderr, "Evaluating sample %u\n", sample);
      Storage_addParameter(mc2->storage, (unsigned int) Workload_idx(workload),
                           (unsigned int) nTrials, v);
      RectRealVectorSpace_discretisedRealVectorIndexes(slice, v, v_discr);
      if (!RealVectorSet_contains(virtualPatients,
                                  sizeof(double) * RealVector_dim(v),
                                  Array_as_C_array(RealVector_as_Array(v)))) {
        char *virtualPatientFileNameTmp = NULL;
        fprintf(stderr, "Start Checker (estimated waiting time: %gs)\n", mc2->sum_checker_run_time / ((double) nTrials + 1));
        Timer_start("MC2_Checker_run");
        Checker *checker = NULL;
        CheckerOutcome admissibility_check_outcome = CHECKER_OUTCOME_ERR;
        double tau = NAN;
        Array *checker_results =
            Checker_admissibility_check(mc2->checkers, v, virtualPatientFileNameTmp,
                                &admissibility_check_outcome, &tau);
        double checker_run_time = TIMER_TO_SEC(Timer_end("MC2_Checker_run"));
        fprintf(stderr, "End Checker (execution time: %gs)\n", checker_run_time);
        mc2->sum_checker_run_time += checker_run_time;
        ++nSimulations;
        switch (admissibility_check_outcome) {
          case CHECKER_OUTCOME_NOT_ADM:
            fprintf(stderr, "Result: NOT ADM\n");
            for (unsigned int i = 0; i < Array_length(mc2->layers); i++) {
              Array_get(mc2->layers, i, &layer);
              MC2Layer_incrFailures(layer);
            }
            break;
          case CHECKER_OUTCOME_ADM:
            fprintf(stderr, "Result: ADM with time-shift %g\n", tau);
            RealVectorSet_add(virtualPatients,
                              sizeof(double) * RealVector_dim(v),
                              Array_as_C_array(RealVector_as_Array(v)), v);
            Storage_addVirtualPatient(mc2->storage,
                                      (unsigned int) Workload_idx(workload),
                                      (unsigned int) nTrials, tau, v);
            update_layers(mc2, workload, nTrials, v_discr, checker_results);
            for (unsigned int j = 0; j < Array_length(checker_results); j++) {
              CheckerResult *res = NULL;
              Array_get(checker_results, j, &res);
              CheckerResult_free(&res);
            }
            Array_free(&checker_results);
            break;
          case CHECKER_OUTCOME_ERR:
            fprintf(stderr, "Result: ERR\n");
            for (unsigned int i = 0; i < Array_length(mc2->layers); i++) {
              Array_get(mc2->layers, i, &layer);
              MC2Layer_incrFailures(layer);
              MC2Layer_incrErrors(layer);
            }
            break;
          default:
            fprintf(stderr, "This never happens\n");
            exit(EXIT_FAILURE);
        }
      } else {
        for (unsigned int i = 0; i < Array_length(mc2->layers); i++) {
          Array_get(mc2->layers, i, &layer);
          MC2Layer_incrFailures(layer);
        }
      }
      for (unsigned int i = 0; i < Array_length(mc2->layers); i++) {
        Array_get(mc2->layers, i, &layer);
        char *layer_str = MC2Layer_toString(layer);
        gzprintf(outputStream, "%llu,%lu,%u,%s\n", Debug_timestamp_millisec(), nTrials, sample, layer_str);
        free(layer_str);
      }
      ++nTrials;
    }
    //end admissibility checks with the current policy
    // make snapshot of the current state
    Storage_endTransaction(mc2->storage);
    mc2->outputStream_count++;
    gzclose(outputStream);
    makeSnapshot(mc2, nTrials, nSimulations,
                 (unsigned int) RealVectorSet_size(virtualPatients));
  }
  // TODO handle Ctrl-C signal
  //  Array_free(&v_discr);
  //  Sampler_free(&sampler);
  //  RectRealVectorSpace_free(&slice);
  //  RealVector_free(&def);
  //  RealVectorSet_free(&virtualPatients);
  //  free(idx_str);
}

static void makeSnapshot(MC2 *mc2, unsigned long nTrials,
                         unsigned long nSimulations,
                         unsigned int virtualPatients) {
  fprintf(stderr, "\n\t\tI am going to take a snapshot, say \"Cheese\"!\n");
  Array *mc2Layers = mc2->layers;
  MC2Layer *layer = NULL;
  for (unsigned int i = 0; i < Array_length(mc2Layers); i++) {
    Array_get(mc2Layers, i, &layer);
    Properties *info = MC2Layer_info(layer);
    char idx_str[FILENAME_MAX] = {0};
    sprintf(idx_str, "snapshot/layers/%u", i);
    Properties_setNestedProperties(mc2->options, idx_str, info);
    Properties_free(&info);
  }
  Properties_setUInt(mc2->options, "snapshot/outputStream_count", mc2->outputStream_count);
  Properties_setUInt(mc2->options, "snapshot/virtualPatients", virtualPatients);
  Properties_setULong(mc2->options, "snapshot/nTrials", nTrials);
  Properties_setULong(mc2->options, "snapshot/nSimulations", nSimulations);
  Properties_setULong(mc2->options, "snapshot/seed",
                      RndGen_getSeedUpcomingSequence(mc2->rndGen));
  Properties_setDouble(mc2->options, "snapshot/walltime",
                       TIMER_TO_SEC(Timer_getCurrentValue(MC2_TIME)));
  Properties_save(mc2->options, mc2->snapshotFilename);
  fprintf(stderr, "\t\t*** Shutter Sound ***\n\n");
}


static void update_layers(MC2 *mc2, Workload const *workload, unsigned long nTrials, Array *v_discr, Array *checker_results) {
  Fingerprint *fingerprint = NULL;
  MC2Layer *layer = NULL;
  FilterResult *filter_res = Filter_filter(mc2->filter, v_discr, checker_results);
  Array_get(mc2->layers, 0, &layer);
  MC2Layer_incrAdm(layer);
  // layers > 0 need to be checked also with fingerprint
  for (unsigned int i = 1; i < Array_length(mc2->layers); i++) {
    Array_get(mc2->layers, i, &layer);
    Array_get(FilterResult_fingerprints(filter_res), i-1, &fingerprint);
    double distance = NAN;
    Array_get(FilterResult_distances(filter_res), i-1, &distance);
    MC2Layer_update(layer, fingerprint,
                    (unsigned int) Workload_idx(workload),
                    (unsigned int) nTrials, distance);
  }
  FilterResult_free(&filter_res);
}
