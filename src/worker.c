/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include <limits.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <MultiProcess_Utils.h>

#include "ParameterSpace.h"
#include "Workload.h"
#include "Injections.h"
#include "Simulator.h"
#include "CheckerOutcome.h"
#include "CheckerResult.h"
#include "Checker.h"
#include "MC2.h"
#include "tags.h"
#include "worker.h"

#define DEBUG_THIS "Worker"

static Properties *execWorkload(int id, Simulator *simulator, ParameterSpace *pSpace, double delta, Properties const *options, Workload const *workload) {
  unsigned long sliceIdx = Workload_idx(workload);
  char const *output;
  int err = Properties_getString(options, "output", &output);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'output' is missing in options");
  char outputPath[PATH_MAX];
  snprintf(outputPath, PATH_MAX, "%s/%lu", output, sliceIdx);
  err = mkdir(outputPath, S_IRWXU | S_IRWXG | S_IRWXO);
  int errno_r = errno;
  Debug_assert(DEBUG_ALWAYS, errno_r == EEXIST || err == 0, "Cannot create output directory for worker #%d\n", id);

  char logPath[PATH_MAX];
  snprintf(logPath, PATH_MAX, "%s/%lu/log.txt", output, sliceIdx);
  FILE *logStream = NULL;
  Properties *snapshot = NULL;
  if (errno_r == EEXIST) {
    char *snapshotFile = NULL;
    asprintf(&snapshotFile, "%s/%lu/snapshot.prop.txt", output, sliceIdx);
    snapshot = Properties_newFromFile(snapshotFile);
    logStream = fopen(logPath, "a");
    dup2(fileno(logStream), STDOUT_FILENO);
    dup2(fileno(logStream), STDERR_FILENO);
    fclose(logStream);
    fprintf(stderr, "[Worker %d] Recover slice %lu from snapshot file %s\n", id, sliceIdx, snapshotFile);
  } else {
    logStream = fopen(logPath, "w");
    dup2(fileno(logStream), STDOUT_FILENO);
    dup2(fileno(logStream), STDERR_FILENO);
    fclose(logStream);
  }

  Properties *checkerOptions = NULL;
  err = Properties_getNestedProperties(snapshot == NULL ? options : snapshot, "checkerOptions", &checkerOptions);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'checkerOptions' is missing in %s", snapshot == NULL ? "options" : "snapshot");

  Array *treatments = Array_new(1, sizeof(Injections *), 1, NULL, NULL);
  Properties *injectionsOptions = NULL;
  err = Properties_getNestedProperties(snapshot == NULL ? options : snapshot, "injectionsOptions", &injectionsOptions);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'injectionsOptions' is missing in %s", snapshot == NULL ? "options" : "snapshot");
  Array *checkers = Array_new(Array_length(treatments), sizeof(Injections *), 1, NULL, NULL);
  Checker *checker =
      Checker_new(pSpace, simulator, NULL, checkerOptions);
  Array_add(checkers, &checker);
  for (unsigned int i = 0; i < Properties_size(injectionsOptions); i++) {
    char const *injections_path = NULL;
    if (i > 0) { // the first element is without injections
      char *idx = NULL;
      asprintf(&idx, "%u", i);
      Properties_getString(injectionsOptions, idx, &injections_path);
      free(idx);
    }
    Checker *checker = Checker_new(pSpace, simulator, injections_path, checkerOptions);
    Array_add(checkers, &checker);
  }
  Array_free(&treatments);
  MC2 *mc2 = MC2_new(id, pSpace, checkers, outputPath, delta, (Properties const *) (snapshot == NULL ? options : snapshot));
  Properties *stats = Properties_new();
  MC2_run(mc2, workload);
  MC2_free(&mc2);
  for (unsigned int i = 0; i <= Array_length(treatments); i++) {
    Checker *checker = NULL;
    Array_get(checkers, i, &checker);
    Checker_free(&checker);
  }
  Array_free(&checkers);
  if (snapshot != NULL) Properties_free(&snapshot);
  return stats;
}

void worker(int id, Properties const *options) {
  /* Debug_enableOut("Sampler"); */
  /* Debug_disableOut("ParameterSpace Verbose"); */
  /* Debug_disableOut("MC2 Verbose"); */
  /* Debug_disableOut("MC2 Verbose RealVectorSet"); */
  /* Debug_disableOut("SignalMetrics Verbose"); */
  /* Debug_disableOut("Checker Cross Correlation"); */
  /* Debug_disableOut("Checker Average"); */
  /* Debug_disableOut("Checker Caching"); */
  /* Debug_disableOut("Checker Verbose"); */
  /* Debug_disableOut("Checker Signature"); */
  /* Debug_disableOut("RealVectorSet"); */
  /* Debug_disableOut("TimeEvolution"); */
  /* Debug_disableOut("TimeEvolution Transformed"); */
  /* Debug_disableOut("Patient"); */
//  Debug_enableOut("Filter Fingerprint");
  /* Debug_disableOut("AlignmentsIterator"); */
  char const *notifyCmd = NULL;
  int err = Properties_getString(options, "notifications/cmd", &notifyCmd);
  int notifyStep = -1;
  err = Properties_getInt(options, "notifications/step", &notifyStep);

  double delta = NAN;
  err = Properties_getDouble(options, "delta", &delta);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'delta' is missing in options");


  HashSet *speciesIdxs = NULL;
  char const *simulatorExecPath;
  err = Properties_getString(options, "simulatorExecPath", &simulatorExecPath);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "'simulatorExecPath' is missing in options");

  char const *simulatorFMUPath = NULL;
  err = Properties_getString(options, "simulatorFMUPath",
                             &simulatorFMUPath);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "'simulatorFMUPath' is missing in options");
  Simulator *simulator =
      Simulator_new(simulatorExecPath, simulatorFMUPath);

  Checker *checker = NULL;
  ParameterSpace *pSpace = NULL;
  MC2 *mc2 = NULL;

  MultiProcess_Message_Out *msgOut = NULL;
  MultiProcess_Message_In *msgIn = NULL;
  int tag = -1;
  int sender = -1;
  while (true) {
    msgIn = MultiProcess_Message_In_new(0, MPI_ANY_TAG);
    MultiProcess_Message_In_waitFor(msgIn);
    tag = MultiProcess_Message_In_tag(msgIn);
    sender = MultiProcess_Message_In_sender(msgIn);
    if (tag == MPI_TAG_START) {
      pSpace = MultiProcess_Utils_new_Obj_from_Message_In(msgIn);
      Debug_out(DEBUG_THIS, "Parameter space:\n");
      Debug_perform(DEBUG_THIS, ParameterSpace_fprint(pSpace, stderr));
      msgOut = MultiProcess_Message_Out_new(MPI_TAG_READY, 0, 0, NULL, BASIC_TYPE_INT, 0);
      MultiProcess_Message_Out_waitFor(msgOut);
      MultiProcess_Message_Out_free(&msgOut);
    } else if (tag == MPI_TAG_WORKLOAD) {
      Workload *workload = MultiProcess_Utils_new_Obj_from_Message_In(msgIn);
      Properties *stats = execWorkload(id, simulator, pSpace, delta, options, workload);
      char *stats_str = Properties_toString(stats, 1024);
      Array *stats_arr = Array_new_wrap((unsigned int) strlen(stats_str) + 1, sizeof(char), stats_str, 1, NULL, NULL);
      Workload_free(&workload);
      msgOut = MultiProcess_Message_Out_new(MPI_TAG_READY, 0, 0, stats_arr, BASIC_TYPE_CHAR, 0);
      Array_free(&stats_arr);
      Properties_free(&stats);
      MultiProcess_Message_Out_waitFor(msgOut);
      MultiProcess_Message_Out_free(&msgOut);
    } else if (tag == MPI_TAG_FINISH) {
      Debug_out(DEBUG_THIS, "Everything done\n");
      msgOut = MultiProcess_Message_Out_new(MPI_TAG_FINISH, 0, 0, NULL, BASIC_TYPE_INT, 0);
      MultiProcess_Message_Out_waitFor(msgOut);
      MultiProcess_Message_Out_free(&msgOut);
    } else if (tag == MPI_TAG_EXIT) {
      Debug_out(DEBUG_THIS, "Exiting\n");
      break;
    } else {
      Debug_assert(DEBUG_ALWAYS, false, "Unexpected MPI message tag.");
    }
    MultiProcess_Message_In_free(&msgIn);
  }
  ParameterSpace_free(&pSpace);
  Simulator_free(&simulator);
  HashSet_free(&speciesIdxs);
  Debug_out(DEBUG_THIS, "Everything done\n");
}
