/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#define DEBUG "TimeEvolution Transformed"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <MCLabUtils.h>

#include "TimeEvolution.h"
#include "TimeEvolution_Transformed.h"

struct TimeEvolution_Transformed {
  // for all t:  this(t) = te( alpha * t + tau)
  TimeEvolution *te;
  double alpha;
  double tau;
};

void TimeEvolution_Transformed_fprint(TimeEvolution_Transformed const *tet, FILE *f) {
  // fprintf(f, "alpha: %g, tau: %g\n", tet->alpha, tet->tau);
  for (int i = 0; i < TimeEvolution_length(tet->te); i++) {
    fprintf(f, "%.17g, %.17g\n", TimeEvolution_Transformed_timePoint(tet, i), TimeEvolution_Transformed_value(tet, i));
  }
}

TimeEvolution_Transformed *
TimeEvolution_Transformed_new(TimeEvolution *te, double alpha,
                              double tau) {
  Debug_assert(DEBUG, !Numbers_approxEQ(alpha, 0), "alpha == 0\n");
  TimeEvolution_Transformed *new = calloc(1, sizeof(TimeEvolution_Transformed));
  Debug_assert(DEBUG, new != NULL, "new == NULL\n");

  new->te = te;
  new->alpha = alpha;
  new->tau = tau;
  return new;
}

double
TimeEvolution_Transformed_minTime(TimeEvolution_Transformed *this) {
  return this->alpha * TimeEvolution_minTime(this->te) + this->tau;
}

double
TimeEvolution_Transformed_maxTime(TimeEvolution_Transformed *this) {
  return this->alpha * TimeEvolution_maxTime(this->te) + this->tau;
}

void TimeEvolution_Transformed_free( TimeEvolution_Transformed **thisP) {
  Debug_assert(DEBUG, thisP != NULL, "thisP == NULL\n");
  TimeEvolution_Transformed *this = *thisP;
  if (this == NULL) return;
  free(this);
  *thisP = NULL;
}

double TimeEvolution_Transformed_avg(TimeEvolution_Transformed *this) {
  return TimeEvolution_avg(this->te);
}

double TimeEvolution_Transformed_min(TimeEvolution_Transformed *this) {
  return TimeEvolution_min(this->te);
}

double TimeEvolution_Transformed_max(TimeEvolution_Transformed *this) {
  return TimeEvolution_max(this->te);
}

int TimeEvolution_Transformed_nbNaN(
    TimeEvolution_Transformed const *this) {
  return TimeEvolution_nbNaN(this->te);
}

int TimeEvolution_Transformed_length(
    TimeEvolution_Transformed const *this) {
  return TimeEvolution_length(this->te);
}

double TimeEvolution_Transformed_findArgMaxWithinInterval(TimeEvolution_Transformed *this, double startTime, double stopTime) {
  double argMax = 0.0;
  double max = 0.0;
  for (int i = 0; i < TimeEvolution_Transformed_length(this); i++) {
    if (Numbers_approxG(TimeEvolution_Transformed_timePoint(this, i), stopTime)) break;
    if (Numbers_approxGE(TimeEvolution_Transformed_timePoint(this, i), startTime)) {
      double value = TimeEvolution_Transformed_value(this, i);
      // fprintf(stderr, "starttime %g stoptime %g value %g max %g argmax %g\n", startTime, stopTime, value, max, argMax);
      if (Numbers_approxG(value, max)) {
        argMax = TimeEvolution_Transformed_timePoint(this, i);
        max = value;
      }
    }
  }
  return argMax;
}

double
TimeEvolution_Transformed_timePoint(TimeEvolution_Transformed const *this,
                                    int i) {
  double te_timePoint = TimeEvolution_timePoint(this->te, i);
  /* fprintf(stderr, "(alpha:%g * (te:%g = TimeEvolution_timePoint(i:%d) + tau:%g)):%g\n", this->alpha, te_timePoint, i, this->tau, this->alpha * (te_timePoint + this->tau)); */
  return (te_timePoint - this->tau) / this->alpha;
}
double
TimeEvolution_Transformed_value(TimeEvolution_Transformed const *this,
                                int i) {
  return TimeEvolution_value(this->te, i);
}

int TimeEvolution_Transformed_lastSampleBefore(
    TimeEvolution_Transformed const *this, double t) {
  /* tet(t) == te(alpha (t' + tau) )
    --> t' = t/alpha - tau
  */
  /* fprintf(stderr, "tet lastSampleBefore: alpha:%g * ( t:%g + tau:%g)\n", this->alpha, t, this->tau); */
  /* return TimeEvolution_lastSampleBefore(this->te, t / this->alpha - this->tau); */
  return TimeEvolution_lastSampleBefore(this->te, this->alpha * t + this->tau);
}

double TimeEvolution_Transformed_Interpolation_value(
    TimeEvolution_Transformed const *this, double t) {
  /* return TimeEvolution_Interpolation_value(this->te, */
  /*                                          t / this->alpha - this->tau); */
  return TimeEvolution_Interpolation_value(this->te,
                                           this->alpha * t + this->tau);
}
