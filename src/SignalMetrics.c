/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>

#include <MCLabUtils.h>

#include "TimeEvolution.h"
#include "TimeEvolution_Transformed.h"

#include "SignalMetrics.h"

#define DEBUG "SignalMetrics"
#define DEBUG_VERBOSE "SignalMetrics Verbose"

SignalMetrics *SignalMetrics_new() {
  SignalMetrics *s = calloc(1, sizeof(SignalMetrics));
  return s;
}

void SignalMetrics_free(SignalMetrics **sP) {
  Debug_assert(DEBUG_ALWAYS, sP != NULL, "sP == NULL\n");
  if (*sP == NULL) return;
  free(*sP);
}

void SignalMetrics_compute(SignalMetrics *s,
                           TimeEvolution_Transformed const *tet1,
                           TimeEvolution_Transformed const *tet2,
                           Range_Double *timeRange) {
  Debug_assert(DEBUG_ALWAYS, tet1 != NULL, "tet1 == NULL\n");
  Debug_assert(DEBUG_ALWAYS, tet2 != NULL, "tet2 == NULL\n");
  Debug_assert(DEBUG_ALWAYS, timeRange != NULL, "timeRange == NULL\n");


  double zero_lag_cross_corr_tet2_tet2 = 0.0;
  double zero_lag_cross_corr_tet1_tet2 = 0.0;
  double avg = 0.0;
  double energy = 0.0;
  double sample_var = 0.0;
  double min = INFINITY;
  double max = -INFINITY;
  double t = 0.0;
  double len = (double) Range_Double_size(timeRange);

  Range_Double_Iterator *it = Range_Double_Iterator_new(timeRange);
  while (Range_Double_Iterator_next(it, &t)) {
    int i1 = TimeEvolution_Transformed_lastSampleBefore(tet1, t);
    Debug_assert(DEBUG_ALWAYS, i1 >= 0, "Index of %g out of bound (< 0)\n", t);
    double t1 = TimeEvolution_Transformed_timePoint(tet1, i1);
    double value1 = TimeEvolution_Transformed_value(tet1, i1);
    Debug_out(DEBUG_VERBOSE, "tet1(%d:%g)=%g\n", i1, t1, value1);

    int i2 = TimeEvolution_Transformed_lastSampleBefore(tet2, t);
    Debug_assert(DEBUG_ALWAYS, i2 >= 0, "Index of %g out of bound (< 0)\n", t);
    double t2 = TimeEvolution_Transformed_timePoint(tet2, i2);
    double value2 = TimeEvolution_Transformed_value(tet2, i2);
    Debug_out(DEBUG_VERBOSE, "tet2(%d:%g)=%g\n", i2, t2, value2);

    zero_lag_cross_corr_tet1_tet2 += value1 * value2;
    zero_lag_cross_corr_tet2_tet2 += value2 * value2;
    avg += value2;
    sample_var += value2 * value2;
    if (value2 < min) min = value2;
    if (value2 > max) max = value2;
  }
  Range_Double_Iterator_free(&it);

  s->min = min;
  s->max = max;
  s->avg = avg / len;
  // 1/ (t_end - t_start) * sum_from_{t_start}_to_{t_end} of value
  s->sample_var = sample_var / len - s->avg * s->avg;
  // 1 ( t_end - t_start) *
  // sum_from_{t_start}_to_{t_end} of value^2 -
  // avg^2
  s->l2_norm = sqrt(zero_lag_cross_corr_tet2_tet2); // sqrt( <tet2, tet2>(0) )
  s->energy = zero_lag_cross_corr_tet2_tet2; // <tet2, tet2>(0)
  s->zero_lag_cross_corr = zero_lag_cross_corr_tet1_tet2;
}
