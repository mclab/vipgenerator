/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>

#include <MCLabUtils.h>

#include "TimeEvolution.h"

#include "Patient.h"

#define DEBUG "Patient"

struct Patient {
  unsigned int numSpecies;
  Array *timeEvolutions; // array of TimeEvolution*
  char *inFilePath;
  bool transientEnd;
  double transientEndValue;
};

Patient *_new(unsigned int numSpecies) {
  Patient *new = calloc(1, sizeof(Patient));
  new->numSpecies = numSpecies;
  new->timeEvolutions =
      Array_new((unsigned int) numSpecies, sizeof(TimeEvolution *),
                (unsigned int) numSpecies, NULL, NULL);
  new->inFilePath = NULL;
  TimeEvolution *tes = NULL;
  for (unsigned int s = 0; s < numSpecies; s++) {
    tes = TimeEvolution_new(100);
    Array_push(new->timeEvolutions, &tes);
  }
  new->transientEnd = false;
  return new;
}

Patient *Patient_newWithTransientPeriod(unsigned int numSpecies, double transientEndValue) {
  Patient *new = _new(numSpecies);
  new->transientEnd = true;
  new->transientEndValue = transientEndValue;
  return new;
}

Patient *Patient_new(unsigned int numSpecies) {
  return _new(numSpecies);
}

unsigned int Patient_numSpecies(Patient const *patient) {
  return Array_length(patient->timeEvolutions);
}

void Patient_free(Patient **thisP) {
  Debug_assert(DEBUG, thisP != NULL, "thisP == NULL\n");
  Patient *patient = *thisP;
  if (patient == NULL) return;
  TimeEvolution *tes = NULL;
  for (unsigned int s = 0; s < Array_length(patient->timeEvolutions); s++) {
    Array_get(patient->timeEvolutions, s, &tes);
    TimeEvolution_free(&tes);
  }
  Array_free(&patient->timeEvolutions);
  if (patient->inFilePath != NULL) free(patient->inFilePath);
  free(*thisP);
}

void _addSpeciesValues(Patient *patient, double t, Array *fields, HashSet const *speciesIdxs) {
  char *s = NULL;
  double v = NAN;
  int err = 0;
  unsigned int speciesIdx = 0;
  TimeEvolution *tes = NULL;
  for (unsigned int i = 1; i < Array_length(fields); i++) {
    Array_get(fields, i, &s);
    if (speciesIdxs != NULL &&
        HashSet_contains(speciesIdxs, &i, sizeof(unsigned int))) {
      err = StringUtils_toDouble(s, &v);
      if (err != 0) {free(s); continue;}
      if (Debug_isEnabled(DEBUG, 1)) fprintf(stderr, ", %u:%.17g", i, v);
      Array_get(patient->timeEvolutions, speciesIdx, &tes);
      TimeEvolution_push(tes, t, v);
      speciesIdx++;
    } else if ((speciesIdxs == NULL &&
                (i <= (unsigned int) patient->numSpecies))) {
      err = StringUtils_toDouble(s, &v);
      if (err != 0) {free(s); continue;}
      if (Debug_isEnabled(DEBUG, 1)) fprintf(stderr, ", %u:%.17g", i, v);
      speciesIdx = i - 1;
      Array_get(patient->timeEvolutions, speciesIdx, &tes);
      TimeEvolution_push(tes, t, v);
    }
    free(s);
  }
}


#define MAX_LINE_LEN (10000)
int Patient_load(Patient *patient, char const *inFilePath, HashSet const *speciesIdxs, bool withHeader) {
  TimeEvolution *tes = NULL;
  for (unsigned int s = 0; s < patient->numSpecies; ++s) {
    Array_get(patient->timeEvolutions, s, &tes);
    TimeEvolution_clear(tes);
  }
  if (patient->inFilePath != NULL) free(patient->inFilePath);
  patient->inFilePath = StringUtils_clone(inFilePath);
  CSV *csv = CSV_new(patient->inFilePath, withHeader, ',', MAX_LINE_LEN);
  Debug_assert(DEBUG_ALWAYS, csv != NULL, "Could not open file '%s' as CSV\n", inFilePath);
  Array *fields = NULL;
  char *s = NULL;
  double t = NAN;
  double v = NAN;
  unsigned int speciesIdx = 0;
  int err = 0;
  while ((fields = CSV_nextRow(csv)) != NULL) {
    Array_get(fields, 0, &s);
    StringUtils_toDouble(s, &t);
    if (Debug_isEnabled(DEBUG, 1)) fprintf(stderr, "\ttime:%.17g", t);
    free(s);
    if (patient->transientEnd) {
      if (!Numbers_approxL(t, patient->transientEndValue)) {
        t = t - patient->transientEndValue;
        _addSpeciesValues(patient, t, fields, speciesIdxs);
      } else {
        for (unsigned int i = 1; i < Array_length(fields); i++) {
          Array_get(fields, i, &s);
          free(s);
        }
      }
    } else { _addSpeciesValues(patient, t, fields, speciesIdxs); }
    if (Debug_isEnabled(DEBUG, 1)) fprintf(stderr, "\n");
    Array_free(&fields);
  }
  CSV_free(&csv);
  return 0;
}

bool Patient_hasTransientPeriod(Patient const *p) {
  return p->transientEnd;
}

double Patient_transientPeriod(Patient const *p) {
  return p->transientEndValue;
}

TimeEvolution *Patient_evolution(Patient const *patient, int speciesIdx) {
  TimeEvolution *result = NULL;
  Array_get(patient->timeEvolutions, (unsigned int) speciesIdx - 1, &result);
  return result;
}
