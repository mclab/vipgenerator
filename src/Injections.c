/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

#include <MCLabUtils.h>


#include "Injections.h"

struct Injections {
  Array *t;
  Array *d;
  char *drug;
};

#define DEBUG_THIS "Injections"

Injections *Injections_newFromFile(char const *inj_path) {
  Debug_assert(DEBUG_THIS, inj_path != NULL, "inj_path == NULL\n");
  Injections *inj = calloc(1, sizeof(Injections));
  FILE *s = fopen(inj_path, "r");
  char *line = NULL;
  size_t linecap = 0;
  ssize_t linelen = -1;
  char const *str = NULL;
  Array *tokens = Array_newString(10, 10);
  unsigned int numAdm = 0;
  double tp = NAN;
  double dose = NAN;
  while ((linelen = getline(&line, &linecap, s)) > 0) {
    StringUtils_trim(line);
    if (strlen(line) == 0 || line[0] == '#') {
      // skip empty lines and comments
      continue;
    }
    StringUtils_tokenize(line, " ", tokens);
    if (Array_length(tokens) < 4) {
      Injections_free(&inj);
      goto cleanup;
    }
    Array_dequeue(tokens, &str);
    inj->drug = StringUtils_clone(str);
    Array_dequeue(tokens, &str);
    if (StringUtils_toUInt(str, &numAdm) != 0) {
      fprintf(stderr, "problem in parsing numAdm %s\n", str);
      Injections_free(&inj);
      goto cleanup;
    }
    inj->t = Array_newDouble(numAdm, 1);
    inj->d = Array_newDouble(numAdm, 1);
    if (numAdm * 2 != Array_length(tokens)) {
      fprintf(stderr, "problem in number of tokens %d != %d\n", numAdm*2, Array_length(tokens));
      Injections_free(&inj);
      goto cleanup;
    }
    for (unsigned int i = 0; i < numAdm; ++i) {
      Array_dequeue(tokens, &str);
      if (StringUtils_toDouble(str, &tp) != 0) {
      fprintf(stderr, "problem in parsing tp %s\n", str);
        Injections_free(&inj);
        goto cleanup;
      }
      Array_dequeue(tokens, &str);
      if (StringUtils_toDouble(str, &dose) != 0) {
        fprintf(stderr, "problem in parsing dose %s\n", str);
        Injections_free(&inj);
        goto cleanup;
      }
      Array_add(inj->t, &tp);
      Array_add(inj->d, &dose);
    }
  }
cleanup:
  free(line);
  Array_free(&tokens);
  fclose(s);
  return inj;
}

Injections *Injections_clone(Injections *inj) {
  inj; //to suppress warning
  fprintf(stderr, "Injections_clone(): Not yet implemented!\n");
  return NULL;
}

void Injections_shift(Injections *inj, double t) {
  Debug_assert(DEBUG_ALWAYS, inj != NULL, "inj == NULL\n");
  for (unsigned int i = 0; i < Array_length(inj->t); i++) {
    double tp = NAN;
    Array_get(inj->t, i, &tp);
    tp += t;
    Array_set(inj->t, i, &tp);
  }
}

double Injections_dose(Injections const *inj, unsigned int i) {
  Debug_assert(DEBUG_ALWAYS, inj != NULL, "inj == NULL\n");
  Debug_assert(DEBUG_ALWAYS, i < Array_length(inj->t), "i out of bounds\n");
  double dose = NAN;
  Array_get(inj->d, i, &dose);
  return dose;
}

double Injections_tp(Injections const *inj, unsigned int i) {
  Debug_assert(DEBUG_ALWAYS, inj != NULL, "inj == NULL\n");
  Debug_assert(DEBUG_ALWAYS, i < Array_length(inj->t), "i out of bounds\n");
  double tp = NAN;
  Array_get(inj->t, i, &tp);
  return tp;
}

unsigned int Injections_num(Injections const *inj) {
  Debug_assert(DEBUG_ALWAYS, inj != NULL, "inj == NULL\n");
  return Array_length(inj->t);
}

char const *Injections_drug(Injections const *inj) {
  Debug_assert(DEBUG_ALWAYS, inj != NULL, "inj == NULL\n");
  return inj->drug;
}


void Injections_free(Injections **injP) {
  if (*injP == NULL) return;
  Injections *inj = *injP;
  if (inj->t != NULL) Array_free(&inj->t);
  if (inj->t != NULL) Array_free(&inj->d);
  free(inj->drug);
  free(*injP);
}
