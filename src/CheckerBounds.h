/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once


typedef struct CheckerBounds CheckerBounds;

extern CheckerBounds *CheckerBounds_new(double min, double max);
extern CheckerBounds *CheckerBounds_newFromString(char const *s);
extern double CheckerBounds_min(CheckerBounds const *cb);
extern double CheckerBounds_max(CheckerBounds const *cb);
extern void CheckerBounds_free(CheckerBounds **cbP);
