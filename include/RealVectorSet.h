/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __REAL_VECTOR_SET_H__
#define __REAL_VECTOR_SET_H__

#include <RndGen.h>
#include <RealVector.h>

typedef struct RealVectorSet RealVectorSet;

/* Creates a new empty set of RealVectors */
extern RealVectorSet *RealVectorSet_new();

/* Creates a new empty set of RealVectors.
  Random extraction of elements is enabled. */
extern RealVectorSet *RealVectorSet_new_withRndAccess();

extern int RealVectorSet_contains(RealVectorSet const *set,
                                  size_t signature_size, void const *signature);

/* Adds set 'toAdd' to set 'this'.
 */
extern int RealVectorSet_union(RealVectorSet *set, RealVectorSet const *toAdd);

/* Clones 'v' and adds the clone to set 'this' under signature 'signature'
  Returns 1 is v has been added (no other RealVector with the same signature)
  and 0 otherwise.
*/
extern int RealVectorSet_add(RealVectorSet *const set, size_t signature_size,
                             const void *const signature,
                             const RealVector *const v);

// Frees the content of the set 'set' (also frees memory for the cloned
// RealVectors in the set)
extern void RealVectorSet_clear(RealVectorSet *set);

// Frees the set pointed to by *thisP (also frees memory for the cloned
// RealVectors in the set)
extern void RealVectorSet_free(RealVectorSet **setP);

// Returns the cardinality of 'this'
extern int RealVectorSet_size(const RealVectorSet *const set);

// Extracts a random element from set. Returns 0 on success and -1 if rnd.
// access is not enabled for set
extern int RealVectorSet_random(const RealVectorSet *const set, RndGen *rnd,
                                RealVector *result);

extern void RealVectorSet_fprint(RealVectorSet const *set, FILE *f,
                                 void (*printSignature)(void const *s,
                                                        size_t size, FILE *f));

#endif
