/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdio.h>

#include <Array.h>
#include <RndGen.h>

typedef struct Workload Workload;

extern Workload *Workload_new(unsigned long idx, RndGen *r);
extern Workload *Workload_newWithSeed(unsigned long idx, unsigned long seed);

extern void Workload_free(Workload **workloadPtr);

extern unsigned long Workload_idx(Workload const *workload);

extern char *Workload_idxToString(Workload const *workload);

extern unsigned long Workload_seed(Workload const *workload);

extern Array const *Workload_serialize(void *obj);

extern void *Workload_newFromSerialized(Array const *serialized);

extern void Workload_fprint(Workload const *workload, FILE *s);

extern int const WorkloadStructId;
