/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once


typedef struct Filter Filter;


extern Filter *Filter_new(unsigned int numSpecies, Array *def,
                          Properties const *filterOptions);

extern unsigned int Filter_layers(Filter const *f);
extern void Filter_free(Filter **fP);

extern FilterResult *Filter_filter(Filter const *f, const Array *v,
                                   const Array *checker_results);
