# Main Options
params = data/<parameter space file>

output = <output folder> 

ulong seed = 2144329

# Slicing Options
ulong numSlicesPerSlave = <num slices per worker>

# MC2 Options
double delta = 0.05
uint sampler policy revision = 59914

# Simulator Options
simulatorExecPath = data/model/run.py
simulatorFMUPath = data/model/<FMU.fmu>

# Recovery
ulong recoveryStep = 1

# Filter Options
filterOptions = {
  #Defined by the user 
}

# Checker Options
checkerOptions = {
 # Defined by the user  
}

# file containing injections
injectionsOptions = {
  1 = data/<injection file 1>
  ...
  k = data/<injection file k>
}
