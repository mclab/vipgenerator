/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <CommandLineInterface.h>
#include <Properties.h>
#include <RndGen.h>
#include <MultiProcess_Utils.h>

#include "ParameterSpace.h"
#include "Workload.h"
#include "master.h"
#include "worker.h"


#define DEBUG_THIS "ViPGenerator"

int ViPGenerator_run(int argc, char *argv[]) {
  MultiProcess_Utils_Start(&argc, &argv);
  MultiProcess_Utils_registerTransmissionEnabledStruct(ParameterSpaceStructId, ParameterSpace_serialize, ParameterSpace_newFromSerialized, BASIC_TYPE_DOUBLE);
  MultiProcess_Utils_registerTransmissionEnabledStruct(WorkloadStructId, Workload_serialize, Workload_newFromSerialized, BASIC_TYPE_ULONG);

  CommandLineInterface *cli =
      CommandLineInterface_new(argv[0], "ViPGenerator");
  CommandLineInterface_processArgs(cli, argc, argv);

  Properties const *options = CommandLineInterface_args(cli);
  Debug_assert(DEBUG_ALWAYS, options != NULL, "options == NULL\n");

  int id = MultiProcess_Utils_myID();
  int np = MultiProcess_Utils_nbProcesses();
  Debug_assert(DEBUG_ALWAYS, np >= 2, "Number of processes = %d < 2\n", np);

  if (id == 0) master(np - 1, options);
  else worker(id, options);

  MultiProcess_Utils_End();
  CommandLineInterface_free(&cli);
  Debug_freeAll();
  return 0;
}
