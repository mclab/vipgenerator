/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "Patient.h"

typedef struct Simulator Simulator;

extern Simulator *Simulator_new(char const *execPath, char const *fmuPath);

extern int Simulator_run(Simulator const *sim, RealVector const *v, char const *outputPath,
                  double stopTime, double stepSize, Injections const *injs, Patient *patient);

extern void Simulator_free(Simulator **thisP);
