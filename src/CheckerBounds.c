/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

#include <MCLabUtils.h>
#include <RealVector.h>

#include "Patient.h"
#include "TimeEvolution_Transformed.h"
#include "SignalMetrics.h"

#include "CheckerBounds.h"

#define DEBUG "CheckerBounds"

struct CheckerBounds {
  double min;
  double max;
};

CheckerBounds *CheckerBounds_new(double min, double max) {
  Debug_assert(DEBUG_ALWAYS, min < max, "min >= max\n");
  CheckerBounds *b = calloc(1, sizeof(CheckerBounds));
  b->min = min;
  b->max = max;
  return b;
}

CheckerBounds *CheckerBounds_newFromString(char const *s) {
	Debug_assert(DEBUG_ALWAYS, s != NULL, "s == NULL\n");
	double min = 0;
	double max = 0;
	int tokens = sscanf(s, "[%lf, %lf]", &min, &max);
	return CheckerBounds_new(min, max);
}

double CheckerBounds_min(CheckerBounds const *cb) {
  Debug_assert(DEBUG_ALWAYS, cb != NULL, "cb == NULL\n");
  return cb->min;
}

double CheckerBounds_max(CheckerBounds const *cb) {
  Debug_assert(DEBUG_ALWAYS, cb != NULL, "cb == NULL\n");
  return cb->max;
}

void CheckerBounds_free(CheckerBounds **cbP) {
  Debug_assert(DEBUG_ALWAYS, cbP != NULL, "cbP == NULL\n");
  if (*cbP == NULL) return;
  free(*cbP);
}
