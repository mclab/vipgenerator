/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <libgen.h>
#include <errno.h>

#include <MCLabUtils.h>
#include <RealVector.h>

#include "Injections.h"
#include "Simulator.h"

#define DEBUG "Simulator"
#define INFILE "input.txt"

struct Simulator {
  char *execPath;
  char *tmpDir;
  char *fmu;
};

Simulator *Simulator_new(char const *execPath, char const *fmuPath) {
  Debug_assert(DEBUG, execPath != NULL, "execPath == NULL\n");
  Simulator *sim = calloc(1, sizeof(Simulator));
  sim->execPath = realpath(execPath, NULL);
  sim->tmpDir = StringUtils_concatenate(1, P_tmpdir "/finder.XXXXXX");
  Debug_assert(DEBUG_ALWAYS, sim->tmpDir != NULL, "sim->tmpDir == NULL\n");
  mkdtemp(sim->tmpDir);
  char *fmu = basename((char *) fmuPath);
  sim->fmu = StringUtils_clone(fmu);
  char *fmuAbsPath = realpath(fmuPath, NULL);
  Debug_assert(DEBUG_ALWAYS, fmuAbsPath != NULL, "%s (rel: %s)\n", strerror(errno), fmuPath);
  char *newFMUAbsPath = StringUtils_concatenate(3, sim->tmpDir, "/", fmu);
  Debug_assert(DEBUG_ALWAYS, newFMUAbsPath != NULL, "newConfigXMLPath == NULL\n");
  int res = symlink(fmuAbsPath, newFMUAbsPath);
  Debug_assert(DEBUG_ALWAYS, 0 == res,
               "Could not create symlink of %s to %s\n", fmuAbsPath,
               newFMUAbsPath);
  free(fmuAbsPath);
  free(newFMUAbsPath);
  return sim;
}

void Simulator_free(Simulator **thisP) {
  if (*thisP == NULL) return;
  Simulator *sim = *thisP;
  free(sim->execPath);
  int err = FileUtils_rmdir(sim->tmpDir);
  free(sim->tmpDir);
  free(sim->fmu);
  if (0 != err)
    fprintf(stderr, "Error when trying to remove tmp dir %s\n", sim->tmpDir);
  free(*thisP);
}

int Simulator_run(Simulator const *sim, RealVector const *v, char const *outputPath,
                  double stopTime, double stepSize, Injections const *injs, Patient *patient) {
  int err = 0;
  Debug_assert(DEBUG_ALWAYS, sim != NULL, "sim == NULL\n");
  Debug_assert(DEBUG_ALWAYS, v != NULL, "v == NULL\n");
  char *outputFileName = NULL;
  if (outputPath == NULL) {
    asprintf(&outputFileName, "out.csv");
  } else {
    char buffer[PATH_MAX+1] = {0};
    realpath(outputPath, buffer);
    outputFileName = StringUtils_clone(buffer);
  }
  Debug_assert(DEBUG_ALWAYS, outputFileName != NULL, "outputFileName == NULL\n");
#ifdef SIM_TIMEOUT
#if defined(__APPLE__) && defined(__MACH__)
  char *execCmd = StringUtils_concatenate(7, "gtimeout -s 9 10 python2 \"", sim->execPath, "\" ", sim->fmu," --output \"", outputFileName, "\" --overridefile \"" INFILE "\" > log.txt 2>&1");
#elif defined(__unix__) || defined(__unix) || defined(unix)
  char *execCmd = StringUtils_concatenate(7, "timeout -s 9 10 python2 \"", sim->execPath, "\" ", sim->fmu," --output \"", outputFileName, "\" --overridefile \"" INFILE "\" > log.txt 2>&1");
#endif
#else
  char *execCmd = StringUtils_concatenate(7, "python2 \"", sim->execPath, "\" ", sim->fmu, " --output \"", outputFileName, "\" --overridefile \"" INFILE "\" > log.txt 2>&1");
#endif
  Debug_assert(DEBUG_ALWAYS, execCmd != NULL, "execCmd == NULL\n");
  char cwd[PATH_MAX] = {0};
  getcwd(cwd, sizeof(cwd));
  chdir(sim->tmpDir);
  FILE *inFile = fopen(INFILE, "w");
  fprintf(inFile, "stopTime=%g\n", stopTime);
  fprintf(inFile, "stepSize=%g\n", stepSize);
  for (unsigned int i = 0; i < RealVector_dim(v); ++i) {
    fprintf(inFile, "p[%u]=%g\n", i + 1, RealVector_get(v, i));
  }
  if (injs != NULL) {
    unsigned int numInjs = Injections_num(injs);
    fprintf(inFile, "%s.num=%u\n", Injections_drug(injs), numInjs);
    for (unsigned int i = 0; i < numInjs; i++) {
      fprintf(inFile, "%s.arr[%u].timing=%g\n", Injections_drug(injs), i+1, Injections_tp(injs, i));
      fprintf(inFile, "%s.arr[%u].dose=%g\n", Injections_drug(injs), i+1, Injections_dose(injs, i));
    }
  }
  fclose(inFile);
  FILE *res = popen(execCmd, "r");
  Debug_out(DEBUG, "Exec: %s in %s\n", execCmd, sim->tmpDir);
  err = pclose(res);
  if (err != -1 && WEXITSTATUS(err) == 0) {
    Patient_load(patient, outputFileName, NULL, false);
    chdir(cwd);
    err = 0;
  } else err = -1;
  chdir(cwd);
  free(execCmd);
  free(outputFileName);
  return err;
}
