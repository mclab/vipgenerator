/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

typedef struct Injections Injections;

extern Injections * Injections_newFromFile(char const *inj_path);
extern Injections *Injections_clone(Injections *inj);
extern void Injections_shift(Injections *inj, double t);
extern double Injections_tp(Injections const *inj, unsigned int i);
extern double Injections_dose(Injections const *inj, unsigned int i);
extern unsigned int Injections_num(Injections const *inj);
extern char const *Injections_drug(Injections const *inj);
extern void Injections_free(Injections **injP);
