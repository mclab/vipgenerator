/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <MCLabUtils.h>
#include <RectRealVectorSpace.h>

#include "RealVectorSet.h"

#include "Patient.h"
#include "CheckerOutcome.h"
#include "CheckerResult.h"
#include "Fingerprint.h"
#include "Storage.h"
#include "MC2Layer.h"

#define DEBUG "MC2Layer"

struct MC2Layer {
  unsigned int id;
  Storage *storage;
  double delta;
  unsigned int nFailures;
  unsigned int nErrors;
  unsigned int nAdm;
  size_t fingerprint_size;
  size_t fingerprint_length;
};

static void initFingerprints(MC2Layer *layer, Fingerprint const *f);

static double computeEpsilon(double delta, unsigned int nFailures) {
  return nFailures == 0 ? 1 : 1 - exp(log(delta)/nFailures);
}

Properties *MC2Layer_info(MC2Layer *const layer) {
  Debug_assert(DEBUG_ALWAYS, layer != NULL, "layer == NULL\n");
  Properties *info = Properties_new();
  Properties_setUInt(info, "id", layer->id);
  Properties_setDouble(info, "delta", layer->delta);
  Properties_setDouble(info, "epsilon", computeEpsilon(layer->delta, layer->nFailures));
  Properties_setUInt(info, "nFailures", layer->nFailures);
  Properties_setUInt(info, "nErrors", layer->nErrors);
  Properties_setUInt(info, "nAdm", layer->nAdm);
  unsigned long fingerprint_size = (unsigned long) layer->fingerprint_size;
  unsigned long fingerprint_length = (unsigned long) layer->fingerprint_length;
  Properties_setULong(info, "fingerprint size", fingerprint_size);
  Properties_setULong(info, "fingerprint length", fingerprint_length);
  return info;
}

MC2Layer *MC2Layer_newFromProperties(Properties const *p, Storage *storage) {
  Debug_assert(DEBUG_ALWAYS, p != NULL,"p == NULL\n");
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  double delta = NAN;
  int err = Properties_getDouble(p, "delta", &delta);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'delta' missing in p\n");
  unsigned int id = 0;
  err = Properties_getUInt(p, "id", &id);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'id' missing in p\n");
  MC2Layer *layer = MC2Layer_new(id, delta, storage);
  err = Properties_getUInt(p, "nFailures", &layer->nFailures);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'nFailures' missing in p\n");
  err = Properties_getUInt(p, "nErrors", &layer->nErrors);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'nErrors' missing in p\n");
  err = Properties_getUInt(p, "nAdm", &layer->nAdm);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'nAdm' missing in p\n");
  unsigned long fingerprint_size = 0;
  unsigned long fingerprint_length = 0;
  err = Properties_getULong(p, "fingerprint size", &fingerprint_size);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'fingerprint size' missing in p\n");
  err = Properties_getULong(p, "fingerprint length", &fingerprint_length);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'fingerprint length' missing in p\n");
  layer->fingerprint_size = (size_t) fingerprint_size;
  layer->fingerprint_length = (size_t) fingerprint_length;
  return layer;
}

MC2Layer *MC2Layer_new(unsigned int id, double delta, Storage *storage) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  MC2Layer *layer = calloc(1, sizeof(MC2Layer));
  layer->storage = storage;
  layer->delta = delta;
  layer->nFailures = 0;
  layer->nErrors = 0;
  layer->nAdm = 0;
  layer->storage = storage;
  layer->id = id;
  return layer;
}

char *MC2Layer_toString(MC2Layer const *layer) {
  Debug_assert(DEBUG_ALWAYS, layer != NULL, "layer == NULL\n");
  char *str = NULL;
  asprintf(&str, "%u,%u,%g", layer->id, layer->nAdm, computeEpsilon(layer->delta, layer->nFailures));
  return str;
}

void MC2Layer_fprint(MC2Layer const *layer, FILE *f) {
  Debug_assert(DEBUG_ALWAYS, layer != NULL, "layer == NULL\n");
  char *str = MC2Layer_toString(layer);
  fprintf(f, "%s", str);
  free(str);
}

void MC2Layer_update(MC2Layer *layer, Fingerprint *f, unsigned int sliceIdx, unsigned int virtualPatientID, double distance) {
  Debug_assert(DEBUG_ALWAYS, layer != NULL, "layer == NULL\n");
  if (Storage_addFingerprint(layer->storage, layer->id, f)) {
    MC2Layer_incrAdm(layer);
  } else {
    MC2Layer_incrFailures(layer);
  }
  Storage_linkFingerprintVirtualPatient(layer->storage, sliceIdx, layer->id, virtualPatientID, f, distance);
}

double MC2Layer_delta(MC2Layer const *layer) {
  Debug_assert(DEBUG_ALWAYS, layer != NULL, "layer == NULL\n");
  return layer->delta;
}

void MC2Layer_incrErrors(MC2Layer *layer) {
  Debug_assert(DEBUG_ALWAYS, layer != NULL, "layer == NULL\n");
  layer->nErrors++;
}

void MC2Layer_incrFailures(MC2Layer *layer) {
  Debug_assert(DEBUG_ALWAYS, layer != NULL, "layer == NULL\n");
  layer->nFailures++;
}

unsigned int MC2Layer_failures(MC2Layer *layer) {
  Debug_assert(DEBUG_ALWAYS, layer != NULL, "layer == NULL\n");
  return layer->nFailures;
}

void MC2Layer_incrAdm(MC2Layer *layer) {
  Debug_assert(DEBUG_ALWAYS, layer != NULL, "layer == NULL\n");
  layer->nAdm++;
  MC2Layer_resetFailures(layer);
  MC2Layer_resetErrors(layer);
}

void MC2Layer_resetFailures(MC2Layer *layer) {
  layer->nFailures = 0;
}

void MC2Layer_resetErrors(MC2Layer *layer) {
  layer->nErrors = 0;
}

void MC2Layer_free(MC2Layer **mc2LayerP) {
  Debug_assert(DEBUG_ALWAYS, mc2LayerP != NULL, "mc2LayerP == NULL\n");
  if (*mc2LayerP == NULL) return;
  MC2Layer *layer = *mc2LayerP;
  free(*mc2LayerP);
}
