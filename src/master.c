/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <limits.h>

#include <unistd.h>

#include <MultiProcess_Utils.h>

#include "ParameterSpace.h"
#include "Workload.h"
#include "tags.h"
#include "master.h"

#define DEBUG_THIS "master"

void master(int numSlaves, Properties const *options) {
  /* Debug_setDefaultOut(0); */
  /* Debug_enableOut("master"); */
  /* Debug_enableOut("ParameterSpace"); */
  int err = 0;

  char const *output = NULL;
  err = Properties_getString(options, "output", &output);
  Debug_assert(DEBUG_ALWAYS, err == 0, "output is missing in options");
  char *logFilepath = NULL;
  asprintf(&logFilepath, "%s/log.txt", output);
  FILE *logStream = fopen(logFilepath, "w");
  dup2(fileno(logStream), STDOUT_FILENO);
  dup2(fileno(logStream), STDERR_FILENO);
  fclose(logStream);

  MultiProcess_Message_Out *msgOut = NULL;
  MultiProcess_Message_In *msgIn = NULL;
  int tag = -1;
  int sender = -1;

  unsigned long numSlicesPerSlave = 0;
  err = Properties_getULong(options, "numSlicesPerSlave", &numSlicesPerSlave);
  Debug_assert(DEBUG_ALWAYS, err == 0, "Missing 'numSlicesPerSlave' in options.\n");

  char const *paramsFilePath = NULL;
  err = Properties_getString(options, "params", &paramsFilePath);
  Debug_assert(DEBUG_ALWAYS, err == 0, "Missing 'params' in options.\n");

  RndGen *r = RndGen_new();
  unsigned long seed = 0;
  err = Properties_getULong(options, "seed", &seed);
  if (err == 0) {
    Debug_out(DEBUG_THIS, "Using seed %lu from options.\n", seed);
    RndGen_setSeed(r, seed);
  }

  unsigned long maxNumSlices = numSlicesPerSlave * (unsigned long) numSlaves;
  Debug_out(DEBUG_THIS, "Maximum number of slices: %lu\n", maxNumSlices);

  ParameterSpace *pSpace = ParameterSpace_new(paramsFilePath, maxNumSlices);
  Debug_out(DEBUG_THIS, "Parameter space:\n");
  Debug_perform(DEBUG_THIS, ParameterSpace_fprint(pSpace, stderr));

  for (int i = 0; i < numSlaves; ++i) {
    msgOut = MultiProcess_Utils_new_Obj_Message_Out(MPI_TAG_START, ParameterSpaceStructId, 0, pSpace, i + 1);
    MultiProcess_Message_Out_free(&msgOut);
  }

  Properties *allStats = Properties_new();
  unsigned long sliceIdx = 0;
  unsigned long numSlices = ParameterSpace_getNumSlices(pSpace);
  int numSlavesFinished = 0;
  while (numSlavesFinished < numSlaves) {
    msgIn = MultiProcess_Message_In_new(MPI_ANY_SOURCE, MPI_ANY_TAG);
    MultiProcess_Message_In_waitFor(msgIn);
    MultiProcess_Message_In_waitFor(msgIn);
    tag = MultiProcess_Message_In_tag(msgIn);
    sender = MultiProcess_Message_In_sender(msgIn);
    if (tag == MPI_TAG_READY) {
      Debug_out(DEBUG_THIS, "Slave #%d is ready to process new slice\n", sender);
      if (MultiProcess_Message_In_msg(msgIn) != NULL) {
        Array const *stats_arr = MultiProcess_Message_In_msg(msgIn);
        Properties *stats = Properties_newFromString((char *) Array_as_C_array(stats_arr));
        Debug_out(DEBUG_THIS, "Slave #%d completed a slice with the following stats:\n", sender);
        Debug_perform(DEBUG_THIS, Properties_fprint(stats, stderr));
        unsigned long stats_slice_idx = 0;
        Properties_getULong(stats, "sliceIdx", &stats_slice_idx);
        char stats_slice_idx_s[1024] = {0};
        sprintf(stats_slice_idx_s, "%lu", stats_slice_idx);
        Properties_setNestedProperties(allStats, stats_slice_idx_s, stats);
        Properties_free(&stats);
      }
      if (sliceIdx < numSlices) {
        Workload *workload = Workload_new(sliceIdx, r);
        msgOut = MultiProcess_Utils_new_Obj_Message_Out(MPI_TAG_WORKLOAD, WorkloadStructId, 0, workload, sender);
        MultiProcess_Message_Out_free(&msgOut);
        Workload_free(&workload);
        ++sliceIdx;
      } else { // everything done
        msgOut = MultiProcess_Message_Out_new(MPI_TAG_FINISH, 0, 0, NULL, BASIC_TYPE_INT, sender);
        MultiProcess_Message_Out_free(&msgOut);
      }
    } else if (tag == MPI_TAG_FINISH) {
      Debug_out(DEBUG_THIS, "Slave #%d finished\n", sender);
      ++numSlavesFinished;
    } else {
      Debug_assert(DEBUG_ALWAYS, false, "Unexpected MPI message tag.");
    }
    MultiProcess_Message_In_free(&msgIn);
  }

  // Finish by letting all workers to exit
  for (int i = 0; i < numSlaves; ++i) {
    MultiProcess_Message_Out *messageOut = MultiProcess_Message_Out_new(MPI_TAG_EXIT, 0, 0, NULL, BASIC_TYPE_INT, i + 1);
    MultiProcess_Message_Out_waitFor(messageOut);
    MultiProcess_Message_Out_free(&messageOut);
  }
  char *outputStats = NULL;
  asprintf(&outputStats, "%s/stats.txt", output);
  Properties_save(allStats, outputStats);
  Properties_free(&allStats);
  free(outputStats);
  ParameterSpace_free(&pSpace);
  RndGen_free(&r);
  free(logFilepath);
}

