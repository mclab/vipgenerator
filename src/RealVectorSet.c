/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <RndGen.h>
#include <HashTable.h>

#include <Debug.h>
#include <Array.h>
#include <RealVector.h>

#include <RealVectorSet.h>

#define INDEX_CAPACITY 10000

#define DEBUG "RealVectorSet"

struct RealVectorSet {
  size_t signature_size;
  int size;
  HashTable *vectors; // of [signature C array] --> RealVector*
  Array *index;       // of [int] --> [signature C array] (keys of the "vectors"
                      // HashTable)
};

void RealVectorSet_fprint(RealVectorSet const *this, FILE *f,
                          void (*printSignature)(void const *s, size_t size,
                                                 FILE *f)) {
  Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
  Debug_assert(DEBUG_ALWAYS, printSignature != NULL,
               "printSignature == NULL\n");
  fprintf(f, "Size: %d\n", this->size);
  fprintf(f, "Signature size: %zu\n", this->signature_size);
  if (this->vectors != NULL) {
    HashTableIterator *it = HashTableIterator_new(this->vectors);
    HashTable_Entry *e = NULL;
    RealVector *v = NULL;
    while (NULL != (e = HashTableIterator_next(it))) {
      printSignature(HashTable_Entry_key(e), this->signature_size, f);
      RealVector_fprint((RealVector *) HashTable_Entry_value(e), f);
    }
    HashTableIterator_free(&it);
    if (this->index != NULL) {
      void *k = NULL;
      for (unsigned int i = 0; i < Array_length(this->index); i++) {
        Array_get(this->index, i, &k);
        fprintf(stderr, "%u --> ", i);
        printSignature(k, this->signature_size, f);
      }
    }
  }
}

RealVectorSet *RealVectorSet_new() {
  RealVectorSet *new = calloc(1, sizeof(RealVectorSet));
  Debug_assert(DEBUG_ALWAYS, new != NULL, "new == NULL");
  new->vectors = NULL;
  new->size = 0;
  new->index = NULL;
  return new;
}

RealVectorSet *RealVectorSet_new_withRndAccess() {
  RealVectorSet *new = RealVectorSet_new();
  new->index =
      Array_new(INDEX_CAPACITY, sizeof(void *), INDEX_CAPACITY, NULL, NULL);
  return new;
}

int RealVectorSet_contains(RealVectorSet const *this, size_t signature_size,
                           void const *signature) {
  Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
  Debug_assert(DEBUG_ALWAYS, this->signature_size == signature_size,
               "this->signature_size = %lu != signature_size = %lu\n",
               this->signature_size, signature_size);
  void *res = NULL;
  HashTable_get(this->vectors, signature, this->signature_size, &res);
  return res != NULL;
}

int RealVectorSet_union(RealVectorSet *this, RealVectorSet const *toAdd) {
  if (this->signature_size != toAdd->signature_size) return -1;
  HashTableIterator *it = HashTableIterator_new(toAdd->vectors);
  HashTable_Entry *e = NULL;
  while (NULL != (e = HashTableIterator_next(it))) {
    RealVectorSet_add(this, this->signature_size, HashTable_Entry_key(e), HashTable_Entry_value(e));
  }
  HashTableIterator_free(&it);
  return 0;
}

int RealVectorSet_add(RealVectorSet *const this, size_t signature_size,
                      const void *const signature, const RealVector *const v) {
  // first insertion
  if (this->vectors == NULL) {
    this->signature_size = signature_size;
    this->vectors = HashTable_new();
  } else {
    Debug_assert(DEBUG_ALWAYS, this->signature_size == signature_size,
                 "this->signature_size = %lu != signature_size = %lu\n",
                 this->signature_size, signature_size);
  }

  // v must be cloned, as RealVectors are "reused"
  HashTable_Entry *e = HashTable_entry(this->vectors, signature, this->signature_size);
  int result = 0;
  if (e == NULL) {
    // Clone vector and add it to set
    RealVector *v_clone = RealVector_clone(v);
    HashTable_put(this->vectors, signature, this->signature_size, v_clone);
    if (this->index != NULL) {
      HashTable_Entry *e_new = HashTable_entry(this->vectors, signature, this->signature_size);
      Debug_assert(DEBUG, e_new != NULL,
                   "No entry found, but value just added!\n");
      const void *actual_signature_ptr = HashTable_Entry_key(e_new);
      Array_add(this->index, &actual_signature_ptr);
    }

    // Debug
    HashTable_Entry *e_new = HashTable_entry(this->vectors, signature, this->signature_size);
    Debug_assert(
        DEBUG, e_new != NULL,
        "Could not find entry, but value just added\n");
    Debug_out(DEBUG, "Key in hashtable = %p\n",
              HashTable_Entry_key(e_new));


    this->size++;
    result = 1;
  } else {
    Debug_out(DEBUG, "NOT added. key in hashtable = %p\n",
              HashTable_Entry_key(e));
    result = 0;
  }
  return result;
}

int RealVectorSet_random(const RealVectorSet *const this, RndGen *rnd,
                         RealVector *result) {
  if (this->index == NULL) return -1;
  if (this->size == 0) return -2;
  int i = (int) RndGen_nextUL(rnd, 0, (unsigned long) this->size - 1);
  void *key = NULL;
  Array_get(this->index, (unsigned int) i, &key);
  RealVector *v = NULL;
  HashTable_get(this->vectors, key, this->signature_size, (void **) &v);
  Debug_assert(DEBUG_ALWAYS, v != NULL, "v == NULL\n");
  RealVector_copy(v, result);
  return 0;
}

void RealVectorSet_clear(RealVectorSet *this) {
  RealVector *v = NULL;
  if (this->vectors != NULL) {
    HashTableIterator *it = HashTableIterator_new(this->vectors);
    HashTable_Entry *e = NULL;
    while ((e = HashTableIterator_next(it)) != NULL) {
      v = (RealVector *) HashTable_Entry_value(e);
      RealVector_free(&v);
    }
    HashTableIterator_free(&it);
    HashTable_free(&this->vectors);
  }
  Array_free(&this->index);
  this->signature_size = 0;
  this->size = 0;
}

void RealVectorSet_free(RealVectorSet **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL");
  RealVectorSet *this = *thisP;
  if (this == NULL) return;
  RealVectorSet_clear(this);
  free(*thisP);
}

int RealVectorSet_size(const RealVectorSet *const this) { return this->size; }
