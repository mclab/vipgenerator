/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

/**
  An instance 'tet' of evol data structure represents a time evolution 'te'
  transformed by a scale factor alpha and a shift factor tau + an offset t0.

  For each t:  tet(t) = te( alpha*(t + tau) + t0)
*/


typedef struct TimeEvolution_Transformed TimeEvolution_Transformed;


extern TimeEvolution_Transformed *
TimeEvolution_Transformed_new(TimeEvolution *te, double alpha, double tau);

extern double
TimeEvolution_Transformed_minTime(TimeEvolution_Transformed *evol);

extern double
TimeEvolution_Transformed_maxTime(TimeEvolution_Transformed *evol);

extern void
TimeEvolution_Transformed_free(TimeEvolution_Transformed **evolP);

extern double
TimeEvolution_Transformed_avg(TimeEvolution_Transformed *evol);

extern double
TimeEvolution_Transformed_min(TimeEvolution_Transformed *evol);

extern double
TimeEvolution_Transformed_max(TimeEvolution_Transformed *evol);

extern int
TimeEvolution_Transformed_nbNaN(TimeEvolution_Transformed const *evol);

extern int
TimeEvolution_Transformed_length(TimeEvolution_Transformed const *evol);

extern double
TimeEvolution_Transformed_timePoint(TimeEvolution_Transformed const *evol,
                                    int i);

double TimeEvolution_Transformed_findArgMaxWithinInterval(TimeEvolution_Transformed *evol, double startTime, double stopTime);

extern double
TimeEvolution_Transformed_value(TimeEvolution_Transformed const *evol, int i);

extern int TimeEvolution_Transformed_lastSampleBefore(
    TimeEvolution_Transformed const *evol, double t);

extern double TimeEvolution_Transformed_Interpolation_value(
    TimeEvolution_Transformed const *evol, double t);

extern void
TimeEvolution_Transformed_fprint(TimeEvolution_Transformed const *tet, FILE *f);
