/*
#    This file is part of ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>

#include <sqlite3.h>
#include <MCLabUtils.h>
#include <RealVector.h>

#include "db.h"
#include "Fingerprint.h"
#include "Storage.h"

#define DEBUG "Storage"

struct Storage {
  StorageMode mode;
  unsigned int virtualPatientLen;
  char *filePath;
  sqlite3 *db;
  bool transaction;

  char *insertVP;
  char *insertParam;
  char *insertHash;
  char *insertFingerprint;
  char *linkFingerprintVP;
  char *containsFingerprint;
  char *containsHash;
};

static void fprintDoubleElem(const void* const elemP, FILE* f) {
	fprintf(f, "%lf", *((double*)elemP));
}

static void StorageMode_fprint(StorageMode mode, FILE *f);

static sqlite3 *initDB(char const *path, unsigned int *virtualPatientLen, StorageMode mode) {
  sqlite3 *db = NULL;
  int status = -1;
  char *errmsg = NULL;
  switch (mode) {
    case STORAGE_MODE_CREATE:
      status = sqlite3_open_v2(path, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
      Debug_assert(DEBUG_ALWAYS, status == SQLITE_OK, "DB %s error %s\n", path, sqlite3_errmsg(db));
      break;
    case STORAGE_MODE_OPEN:
      status = sqlite3_open_v2(path, &db, SQLITE_OPEN_READWRITE, NULL);
      Debug_assert(DEBUG_ALWAYS, status == SQLITE_OK, "DB %s error %s\n", path, sqlite3_errmsg(db));
      break;
  }
  if (mode == STORAGE_MODE_OPEN) {
    char *sql = NULL;
    asprintf(&sql, "select count(*)-3 from pragma_table_info('virtualpatient');");
    sqlite3_stmt *stmt = NULL;
    sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
    int rc = sqlite3_step(stmt);
    Debug_assert(DEBUG_ALWAYS, rc == SQLITE_ROW, "Error: %s\n", sqlite3_errmsg(db));
    *virtualPatientLen = (unsigned int) sqlite3_column_int(stmt, 0);
    sqlite3_finalize(stmt);
    free(sql);
  } else if (mode == STORAGE_MODE_CREATE) {
    sql_exec(db, "PRAGMA foreign_keys = ON;");

    char *sql = NULL;
    sql_exec(db, "create table parameter(slice integer not null, id integer not null, primary key (slice, id));");
    for (unsigned int i = 0; i < *virtualPatientLen; ++i) {
      asprintf(&sql, "alter table parameter add column p%u real;", i);
      sql_exec(db, sql);
      free(sql);
    }

    sql_exec(db, "create table virtualpatient (slice integer not null, id integer not null, tau real not null, primary key (slice, id));");
    for (unsigned int i = 0; i < *virtualPatientLen; ++i) {
      asprintf(&sql, "alter table virtualpatient add column p%u real;", i);
      sql_exec(db, sql);
      free(sql);
    }

    char *pre = NULL;
    asprintf(&sql, "create table fingerprint (layer integer not null, value blob not null, primary key (layer, value));");
    sql_exec(db, sql);
    free(sql);

    pre = NULL;
    asprintf(&sql, "create table cluster (layer integer not null, fingerprint blob not null,"
                   "slice integer not null, virtualpatient integer not null, distance real not null,"
                   "primary key (layer, fingerprint, slice, virtualpatient),"
                   "foreign key (layer, fingerprint) references fingerprint(layer, value),"
                   "foreign key (slice, virtualpatient) references virtualpatient(slice, id));");
    sql_exec(db, sql);
    free(sql);
  }
  return db;
}

Storage *Storage_new(unsigned int virtualPatientLen, char const *path, StorageMode mode) {
  Storage *storage = calloc(1, sizeof(Storage));
  storage->mode = mode;
  storage->virtualPatientLen = virtualPatientLen;
  storage->filePath = StringUtils_clone(path);
  storage->transaction = false;
  storage->db = NULL;
  storage->db = initDB(path, &storage->virtualPatientLen, mode);
  return storage;
}

void Storage_beginTransaction(Storage *storage) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  Debug_assert(DEBUG_ALWAYS, !storage->transaction, "Transaction already in progress.\n");
  sql_exec(storage->db, "begin transaction;");
  storage->transaction = true;
}

void Storage_endTransaction(Storage *storage) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  Debug_assert(DEBUG_ALWAYS, storage->transaction, "No transaction in progress.\n");
  sql_exec(storage->db, "end transaction;");
  storage->transaction = false;
}

Array /*RealVector*/ *Storage_getVirtualPatientParametersOfSlice(Storage const *storage, unsigned int sliceIdx) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  char *sql = NULL;
  asprintf(&sql, "select * from virtualpatient where slice = ? order by id;");
  sqlite3_stmt *stmt = NULL;
  sqlite3_prepare_v2(storage->db, sql, -1, &stmt, NULL);
  int rc = sqlite3_bind_int(stmt, 1, (int) sliceIdx);
  Debug_assert(DEBUG_ALWAYS, rc == SQLITE_OK, "Error: %s\n", sqlite3_errmsg(storage->db));
  rc = sqlite3_step(stmt);
  Array *virtualPatients = Array_new(10000, sizeof(RealVector *), 10000, NULL, NULL);
  while (rc == SQLITE_ROW) {
    Debug_assert(DEBUG_ALWAYS, sqlite3_column_count(stmt) == (int) storage->virtualPatientLen + 3, "%d <> %u", sqlite3_column_count(stmt), storage->virtualPatientLen + 3);
    RealVector *r = RealVector_new(storage->virtualPatientLen);
    int iCol = 0;
    for (unsigned int i = 0; i < storage->virtualPatientLen; i++) {
      iCol = (int) i + 3;
      RealVector_set(r, i, sqlite3_column_double(stmt, iCol));
    }
    Array_add(virtualPatients, &r);
    rc = sqlite3_step(stmt);
  }
  sqlite3_finalize(stmt);
  free(sql);
  return virtualPatients;
}

struct StorageIterator {
  Storage const *storage;
  sqlite3_stmt *stmt;
  char *sql;
  RealVector *next;
};
RealVector *_StorageIterator_next(StorageIterator *it);

StorageIterator *StorageIterator_new(Storage const *storage, char *sql_query) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  StorageIterator *it = calloc(1, sizeof(StorageIterator));
  it->storage = storage;
  it->sql = NULL;
  asprintf(&(it->sql), "%s", sql_query);
  it->stmt = NULL;
  sqlite3_prepare_v2(storage->db, it->sql, -1, &(it->stmt), NULL);
  it->next = _StorageIterator_next(it);
  return it;
}

RealVector *StorageIterator_next(StorageIterator *it) {
  if (it->next == NULL) return NULL;
  RealVector *cache_next = it->next;
  it->next = _StorageIterator_next(it);
  return cache_next;
}

bool StorageIterator_hasNext(StorageIterator *it) { return it->next != NULL; }

RealVector *_StorageIterator_next(StorageIterator *it) {
  sqlite3_stmt *stmt = it->stmt;
  Storage const *storage = it->storage;
  int rc = sqlite3_step(stmt);
  if (rc == SQLITE_ROW) {
    unsigned int columns = (unsigned int) sqlite3_column_count(stmt);
    RealVector *r = RealVector_new(columns);
    for (unsigned int i = 0; i < columns; i++) {
      RealVector_set(r, i, sqlite3_column_double(stmt, (int) i));
    }
    return r;
  }
  return NULL;
}

void StorageIterator_free(StorageIterator **itP) {
  Debug_assert(DEBUG_ALWAYS, itP != NULL, "itP == NULL\n");
  if (*itP == NULL) return;
  StorageIterator *it = *itP;
  sqlite3_finalize(it->stmt);
  free(it->sql);
  free(*itP);
}

void Storage_addParameter(Storage *storage, unsigned int sliceIdx, unsigned int id, RealVector const *v) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  Debug_assert(DEBUG_ALWAYS, v != NULL, "v == NULL\n");
  Debug_assert(DEBUG_ALWAYS, RealVector_dim(v) == storage->virtualPatientLen, "Wrong number of parameters in v\n");
  if (storage->insertParam == NULL) {
    asprintf(&storage->insertParam, "insert into parameter(slice, id");
    char *pre = NULL;
    for (unsigned int i = 0; i < storage->virtualPatientLen; ++i) {
      pre = storage->insertParam;
      asprintf(&storage->insertParam, "%s, p%u", pre, i);
      free(pre);
    }
    pre = storage->insertParam;
    asprintf(&storage->insertParam, "%s) values (?, ?", pre);
    free(pre);
    for (unsigned int i = 0; i < storage->virtualPatientLen; ++i) {
      pre = storage->insertParam;
      asprintf(&storage->insertParam, "%s, ?", pre);
      free(pre);
    }
    pre = storage->insertParam;
    asprintf(&storage->insertParam, "%s);", pre);
    free(pre);
  }
  sqlite3_stmt *stmt = NULL;
  sqlite3_prepare_v2(storage->db, storage->insertParam, -1, &stmt, NULL);
  int rc = sqlite3_bind_int(stmt, 1, (int) sliceIdx);
  Debug_assert(DEBUG_ALWAYS, rc == SQLITE_OK, "Error: %s\n", sqlite3_errmsg(storage->db));
  rc = sqlite3_bind_int(stmt, 2, (int) id);
  Debug_assert(DEBUG_ALWAYS, rc == SQLITE_OK, "Error: %s\n", sqlite3_errmsg(storage->db));
  for (unsigned int i = 0; i < RealVector_dim(v); i++) {
    rc = sqlite3_bind_double(stmt, (int) i + 3, RealVector_get(v, i));
    Debug_assert(DEBUG_ALWAYS, rc == SQLITE_OK, "Error: %s\n", sqlite3_errmsg(storage->db));
  }
  rc = sqlite3_step(stmt);
  Debug_assert(DEBUG_ALWAYS, rc == SQLITE_DONE, "ERROR inserting data: %s\n", sqlite3_errmsg(storage->db));
  sqlite3_finalize(stmt);
}

void Storage_addVirtualPatient(Storage *storage, unsigned int sliceIdx, unsigned int id, double tau, RealVector const *v) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  Debug_assert(DEBUG_ALWAYS, v != NULL, "v == NULL\n");
  Debug_assert(DEBUG_ALWAYS, RealVector_dim(v) == storage->virtualPatientLen, "Wrong number of parameters in v\n");
  if (storage->insertVP == NULL) {
    asprintf(&storage->insertVP, "insert into virtualpatient (slice, id, tau");
    char *pre = NULL;
    for (unsigned int i = 0; i < storage->virtualPatientLen; ++i) {
      pre = storage->insertVP;
      asprintf(&storage->insertVP, "%s, p%u", pre, i);
      free(pre);
    }
    pre = storage->insertVP;
    asprintf(&storage->insertVP, "%s) values (?, ?, ?", pre);
    free(pre);
    for (unsigned int i = 0; i < storage->virtualPatientLen; ++i) {
      pre = storage->insertVP;
      asprintf(&storage->insertVP, "%s, ?", pre);
      free(pre);
    }
    pre = storage->insertVP;
    asprintf(&storage->insertVP, "%s);", pre);
    free(pre);
  }
  sqlite3_stmt *stmt = NULL;
  sqlite3_prepare_v2(storage->db, storage->insertVP, -1, &stmt, NULL);
  int rc = sqlite3_bind_int(stmt, 1, (int) sliceIdx);
  Debug_assert(DEBUG_ALWAYS, rc == SQLITE_OK, "Error: %s\n", sqlite3_errmsg(storage->db));
  rc = sqlite3_bind_int(stmt, 2, (int) id);
  Debug_assert(DEBUG_ALWAYS, rc == SQLITE_OK, "Error: %s\n", sqlite3_errmsg(storage->db));
  rc = sqlite3_bind_double(stmt, 3, tau);
  Debug_assert(DEBUG_ALWAYS, rc == SQLITE_OK, "Error: %s\n", sqlite3_errmsg(storage->db));
  for (unsigned int i = 0; i < RealVector_dim(v); i++) {
    rc = sqlite3_bind_double(stmt, (int) i + 4, RealVector_get(v, i));
    Debug_assert(DEBUG_ALWAYS, rc == SQLITE_OK, "Error: %s\n", sqlite3_errmsg(storage->db));
  }
  rc = sqlite3_step(stmt);
  Debug_assert(DEBUG_ALWAYS, rc == SQLITE_DONE, "ERROR inserting data: %s\n", sqlite3_errmsg(storage->db));
  sqlite3_finalize(stmt);
}

void Storage_linkFingerprintVirtualPatient(Storage *storage, unsigned int sliceIdx, unsigned int layer, unsigned int id, Fingerprint *fingerprint, double distance) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  Debug_assert(DEBUG_ALWAYS, fingerprint != NULL, "fingerprint == NULL\n");
  if (storage->linkFingerprintVP == NULL) {
    asprintf(&storage->linkFingerprintVP, "insert into cluster (layer, fingerprint, slice, virtualpatient, distance)"
                                          " values (?, ?, ?, ?, ?)");
  }
  sqlite3_stmt *stmt = NULL;
  sqlite3_prepare_v2(storage->db, storage->linkFingerprintVP, -1, &stmt, NULL);
  sqlite3_bind_int(stmt, 1, (int) layer);
  uint64_t size = 0;
  int32_t const *fingerprint_values = Fingerprint_trim(fingerprint, &size);
  sqlite3_bind_blob64(stmt, 2, fingerprint_values, size, NULL);
  sqlite3_bind_int(stmt, 3, (int) sliceIdx);
  sqlite3_bind_int(stmt, 4, (int) id);
  sqlite3_bind_double(stmt, 5, distance);
  int rc = sqlite3_step(stmt);
  Debug_assert(DEBUG_ALWAYS, rc == SQLITE_DONE, "ERROR inserting data: %s (\n%s\n)\n", sqlite3_errmsg(storage->db), sqlite3_expanded_sql(stmt));
  sqlite3_finalize(stmt);
}

bool Storage_addFingerprint(Storage *storage, unsigned int layer, Fingerprint *fingerprint) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  Debug_assert(DEBUG_ALWAYS, fingerprint != NULL, "fingerprint == NULL\n");
  if (storage->insertFingerprint == NULL) {
    asprintf(&storage->insertFingerprint, "insert into fingerprint (layer, value) values (?, ?);");
  }
  sqlite3_stmt *stmt = NULL;
  sqlite3_prepare_v2(storage->db, storage->insertFingerprint, -1, &stmt, NULL);
  sqlite3_bind_int(stmt, 1, (int) layer);
  uint64_t size = 0;
  int32_t const *fingerprint_values = Fingerprint_trim(fingerprint, &size);
  sqlite3_bind_blob64(stmt, 2, fingerprint_values, size, NULL);
  int rc = sqlite3_step(stmt);
  sqlite3_finalize(stmt);
  Debug_assert(DEBUG_ALWAYS, rc == SQLITE_DONE || rc == SQLITE_CONSTRAINT, "Error inserting fingerprint %s\n", sqlite3_errmsg(storage->db));
  return rc == SQLITE_DONE;
}

bool Storage_containsFingerprint(Storage *storage, unsigned int layer, Fingerprint *fingerprint) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  Debug_assert(DEBUG_ALWAYS, fingerprint != NULL, "fingerprint == NULL\n");
  if (storage->containsFingerprint == NULL) {
    asprintf(&storage->containsFingerprint, "select * from fingerprint where layer = ? and value = ?;");
  }
  sqlite3_stmt *stmt = NULL;
  sqlite3_prepare_v2(storage->db, storage->containsFingerprint, -1, &stmt, NULL);
  sqlite3_bind_int(stmt, 1, (int) layer);
  uint64_t size = 0;
  int32_t const *fingerprint_values = Fingerprint_trim(fingerprint, &size);
  sqlite3_bind_blob64(stmt, 2, fingerprint_values, size, NULL);
  int rc = sqlite3_step(stmt);
  sqlite3_finalize(stmt);
  if (rc == SQLITE_ROW) return true;
  return false;
}

void Storage_free(Storage **storageP) {
  Debug_assert(DEBUG_ALWAYS, storageP != NULL, "storageP == NULL\n");
  if (*storageP == NULL) return;
  Storage *storage = *storageP;
  free(storage->filePath);
  sqlite3_close(storage->db);
  free(storage->insertVP);
  free(storage->insertParam);
  free(storage->insertHash);
  free(storage->insertFingerprint);
  free(storage->linkFingerprintVP);
  free(storage->containsFingerprint);
  free(storage->containsHash);
  free(*storageP);
}

static void StorageMode_fprint(StorageMode mode, FILE *f) {
  switch (mode) {
    case STORAGE_MODE_OPEN:
      fprintf(f, "Mode: OPEN\n");
      break;
    case STORAGE_MODE_CREATE:
      fprintf(f, "Mode: CREATE\n");
      break;
  }
}

void Storage_fprint(Storage *storage, FILE *f) {
  Debug_assert(DEBUG_ALWAYS, storage != NULL, "storage == NULL\n");
  Debug_assert(DEBUG_ALWAYS, f != NULL, "f == NULL\n");
  StorageMode_fprint(storage->mode, f);
}
